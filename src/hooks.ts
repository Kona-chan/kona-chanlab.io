import type {Handle} from '@sveltejs/kit';

export const handle: Handle = async ({request, resolve}) => {
	const response = await resolve(request);
	let lang = 'ru';
	const pathname = request.url.pathname;
	if (pathname.includes('/en/') || pathname.endsWith('/en')) {
		lang = 'en';
	} else if (pathname.includes('/zh/') || pathname.endsWith('/zh')) {
		lang = 'zh';
	} else if (pathname.includes('/ko/') || pathname.endsWith('/ko')) {
		lang = 'ko';
	}

	return {
		...response,
		body: (response.body as string)?.replace('<html>', `<html lang="${lang}">`),
	};
};
