import type {RequestHandler} from '@sveltejs/kit';
import {IndexFileNotFound, loadIndexFile, loadIndexMdMetadata} from '$lib/AudioPlayer/utils';

export const get: RequestHandler = ({params}) => {
	const {album, language} = params;

	try {
		const albumIndex = loadIndexFile(album);
		const indexMetadata = loadIndexMdMetadata(album, language);

		// Manifest must use snake case, disabling eslint rule
		/* eslint-disable @typescript-eslint/naming-convention */
		const manifest = {
			short_name: indexMetadata.title,
			name: indexMetadata.title,
			start_url: `/listen/${album}/${language}/`,
			icons: albumIndex.manifestIcons,
			background_color: albumIndex.themeColor,
			display: 'standalone',
			scope: `/listen/${album}/${language}/`,
			theme_color: albumIndex.themeColor,
			description: indexMetadata.meta.description,
			categories: ['music'],
		};
		/* eslint-enable @typescript-eslint/naming-convention */

		return {body: manifest};
	} catch (error: unknown) {
		if (!(error instanceof IndexFileNotFound)) {
			throw error;
		}
	}
};
