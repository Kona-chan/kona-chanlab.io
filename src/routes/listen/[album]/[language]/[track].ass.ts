import type {RequestHandler} from '@sveltejs/kit';

import {readSync} from 'to-vfile';

export const get: RequestHandler = async ({params}) => {
	const {album, language, track} = params;

	const subtitlesFilename = `content/listen/${album}/${language}/${track}.ass`;
	let subtitlesFile: ReturnType<typeof readSync>;
	try {
		subtitlesFile = readSync(subtitlesFilename, 'utf8');

		return {
			body: subtitlesFile.value,
			headers: {
				'Content-Type': 'text/plain',
			},
		};
	} catch (error: unknown) {
		if ((error as {code: string}).code !== 'ENOENT') {
			throw error;
		}
	}
};
