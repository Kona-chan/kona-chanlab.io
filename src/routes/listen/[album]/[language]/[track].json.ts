import type {RequestHandler} from '@sveltejs/kit';
import type {Root as MdastRoot} from 'mdast';

import {readSync} from 'to-vfile';
import {unified} from 'unified';
import remarkParse from 'remark-parse';
import remark2rehype from 'remark-rehype';
import rehypeStringify from 'rehype-stringify';
import rehypeRaw from 'rehype-raw';
import frontmatter from 'remark-frontmatter';
import yaml from 'js-yaml';
import textr from 'remark-textr';
import type {TrackMetadata} from '$lib/AudioPlayer/types';
import type {Root as SubsrtRoot} from '$lib/unified/subast';
import rehypeMergeSubtitles from '$lib/unified/rehype-merge-subtitles';
import subsrtParse from '$lib/unified/subsrt-parse';
import subastGetTimings from '$lib/unified/subast-get-timings';

class PageNotFound extends Error {}

function ellipses(input: string) {
	return input.replace(/\.{3}/gim, '…');
}

const subsrtParser = unified()
	.use(subsrtParse, {format: 'ass'});

const remarkParser = unified()
	.use(remarkParse) // Parse Markdown to mdast syntax tree
	.use(frontmatter, ['yaml']); // Split front matter block

const runner = unified()
	.use(rehypeMergeSubtitles)
	.use(textr, {plugins: [ellipses]})
	.use(remark2rehype, {allowDangerousHtml: true}) // Mutate to rehype
	.use(rehypeRaw) // Deal with HTML in Markdown
	.use(rehypeStringify); // Stringify hast syntax tree to HTML

type ProcessOptions = {album: string; language: string; track: string};
const process = async ({album, language, track}: ProcessOptions) => {
	// Parse subtitles
	const subtitlesFilename = `content/listen/${album}/${language}/${track}.ass`;
	let subtitlesFile: ReturnType<typeof readSync>;
	try {
		subtitlesFile = readSync(subtitlesFilename, 'utf8');
	} catch (error: unknown) {
		if ((error as {code: string}).code !== 'ENOENT') {
			throw error;
		}
	}

	let timings: Array<[number, number]>;
	let subtitles: SubsrtRoot;

	if (subtitlesFile) {
		subtitles = subsrtParser.parse(subtitlesFile.value) as SubsrtRoot;
		timings = subastGetTimings(subtitles) as Array<[number, number]>;
	}

	// Parse yaml from markdown file
	const lyricsFilename = `content/listen/${album}/${language}/${track}.md`;
	let lyricsFile: ReturnType<typeof readSync>;
	try {
		lyricsFile = readSync(lyricsFilename, 'utf8');
	} catch (error: unknown) {
		throw (error as {code: string}).code === 'ENOENT' ? new PageNotFound() : error;
	}

	// Split markdown file to yaml and content parts
	const tree = remarkParser.parse(lyricsFile);

	if (tree.children.length === 0 || tree.children[0].type !== 'yaml') {
		throw new Error('invalid markdown file');
	}

	const metadata = yaml.load(tree.children[0].value) as TrackMetadata;
	tree.children.shift();

	(tree as MdastRoot & {subtitles: SubsrtRoot}).subtitles = subtitles;

	// Generate html from markdown
	const content: string = runner.stringify(await runner.run(tree as any));

	return {metadata, content, timings};
};

export const get: RequestHandler = async ({params}) => {
	const {album, language, track} = params;

	try {
		return {
			body: await process({album, language, track}),
		};
	} catch (error: unknown) {
		if (!(error instanceof PageNotFound)) {
			throw error;
		}
	}
};
