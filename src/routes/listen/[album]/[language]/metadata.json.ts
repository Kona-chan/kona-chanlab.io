import type {RequestHandler} from '@sveltejs/kit';
import type {AlbumMetadata, TrackMetadata} from '$lib/AudioPlayer/types';

import {IndexFileNotFound, loadIndexFile, loadIndexMd, loadTrackMdMetadata} from '$lib/AudioPlayer/utils';

export const get: RequestHandler = async ({params}) => {
	const {album, language} = params;

	try {
		const albumIndex = loadIndexFile(album);
		const index = await loadIndexMd(album, language);

		const tracks: TrackMetadata[] = albumIndex.tracks.map(trackMetadata => {
			const track = loadTrackMdMetadata(album, language, trackMetadata.page);
			const trackInfo = {
				title: track.title,
				subtitle: track.subtitle,
				...trackMetadata,
				url: `/listen/${album}/${language}/${trackMetadata.page}/`,
			};
			if (!trackMetadata.instrumental) {
				trackInfo.subtitles = `/listen/${album}/${language}/${trackMetadata.page}.ass`;
			}

			return trackInfo;
		});

		const body: AlbumMetadata = {
			...index.metadata,
			about: index.content,
			languages: albumIndex.languages,
			tracks,

			themeColor: albumIndex.themeColor,
			mirrors: albumIndex.mirrors,
			formats: albumIndex.formats,

			spashscreensPath: albumIndex.spashscreensPath,
			icon: albumIndex.icon,
			appleIcon: albumIndex.appleIcon,
			manifestIcons: albumIndex.manifestIcons,

			language,
			baseUrl: `/listen/${album}/`,
			langUrl: `/listen/${album}/${language}/`,
			webmanifest: `/listen/${album}/${language}/manifest.webmanifest`,
		};

		return {body};
	} catch (error: unknown) {
		if (!(error instanceof IndexFileNotFound)) {
			throw error;
		}
	}
};
