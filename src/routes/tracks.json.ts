
import type {RequestHandler} from '@sveltejs/kit';
import {readSync} from 'to-vfile';
import yaml from 'js-yaml';

export type TrackData = {
	title: string;
	releaseDate: string;
	musicBy: string;
	lyricsBy: string;
	performedBy: string;
};

export type AlbumData = {
	title: string;
	releaseDate: string;
	tracks: TrackData[];
};

export type AlbumsData = {
	albums: AlbumData[];
};

export const get: RequestHandler = () => {
	const file = readSync('content/tracks.yaml', 'utf8');

	const metadata = yaml.load(file.value.toString()) as AlbumsData;

	return {body: metadata};
};
