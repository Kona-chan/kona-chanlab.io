
import type {RequestHandler} from '@sveltejs/kit';
import {process, PageNotFound} from '$lib/markdown';

export const get: RequestHandler = async ({params}) => {
	try {
		const {page, language} = params;
		return {body: await process(`content/${page}.${language}.md`, language)};
	} catch (error: unknown) {
		if (!(error instanceof PageNotFound)) {
			throw error;
		}
	}
};
