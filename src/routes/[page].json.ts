
import type {RequestHandler} from '@sveltejs/kit';
import {process, PageNotFound} from '$lib/markdown';

export const get: RequestHandler = async ({params}) => {
	try {
		return {body: await process(`content/${params.page}.md`)};
	} catch (error: unknown) {
		if (!(error instanceof PageNotFound)) {
			throw error;
		}
	}
};
