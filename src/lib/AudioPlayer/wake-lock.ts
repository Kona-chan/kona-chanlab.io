import {browser} from '$app/env';

export const requestWakeLock = (browser && 'wakeLock' in navigator)
	? 	async () => {
		try {
			wakeLock = await navigator.wakeLock.request('screen');
		} catch (error: unknown) {
			if (error instanceof DOMException) {
				console.log(`Failed to request wake lock: ${error.name}, ${error.message}`);
			} else {
				throw error;
			}
		}
	}

	: async () => {/* pass */};

export const releaseWakeLock = (browser && 'wakeLock' in navigator)
	? 	async () => {
		await wakeLock?.release();
		wakeLock = null;
	}
	: async () => {/* pass */};

let wakeLock: WakeLockSentinel = null;
