import {get} from 'svelte/store';
import {
	activeIndex as activeIndexStore, album as albumStore, playPosition as playPositionStore,
	playStatus as playStatusStore, volume as volumeStore, autoplay as autoplayStore,
	bufferedTo as bufferedToStore, nonInstVisu as nonInstVisuStore, repeatTrack as repeatTrackStore,
	PlayStatus,
} from './stores';
import {releaseWakeLock, requestWakeLock} from './wake-lock';
import {formatTrackTitle} from './formatters';
import {browser} from '$app/env';
import {goto} from '$app/navigation';

export enum SkipDirection {
	Previous, Next,
}

export class Player {
	audio: HTMLAudioElement;
	nextAudio: HTMLAudioElement;

	sourceAudio: MediaElementAudioSourceNode;
	sourceNextAudio: MediaElementAudioSourceNode;

	audioContext: AudioContext;

	analyser: AnalyserNode;
	analyserBuffer: Uint8Array;
	barCount: number;
	barMapping: Array<(rawData: Uint8Array) => number>;
	updateProgressId: number;

	extension: string;
	frequencyCutoff: number;

	hasPlayed: boolean;

	constructor(
	) {
		if (browser) {
			this.audio = this._createAudio('channel2');
			this.nextAudio = this._createAudio('channel1');
		}

		this.hasPlayed = false;
	}

	_createAudio(channel: string) {
		const audio = new Audio();
		audio.dataset.channel = channel;
		audio.crossOrigin = 'anonymous';
		audio.volume = this.volume;
		volumeStore.subscribe(newVolume => {
			audio.volume = newVolume;
		});

		audio.addEventListener('canplaythrough', () => {
			if (!this.currentSong) {
				console.log(audio.dataset.channel, 'no current song!!');
				return;
			}

			console.log(audio.dataset.channel, 'oncanplaythrough', this.audio.duration);
			this.bufferedTo = this.audio.duration;
		});

		audio.addEventListener('ended', () => {
			console.log(audio.dataset.channel, 'onended');

			if (!this.autoplay) {
				return;
			}

			if (this.repeatTrack) {
				this.seek(0);
				this.play();
			} else {
				this.playStatus = PlayStatus.Ended;
				void releaseWakeLock();
				setMediaSessionPlaybackState('none');
				this.skip(SkipDirection.Next);
			}
		});

		audio.addEventListener('load', () => {
			console.log(audio.dataset.channel, 'onload');
		});

		audio.addEventListener('pause', () => {
			console.log(audio.dataset.channel, 'onpause');
			this.playStatus = PlayStatus.Paused;
			void releaseWakeLock();
			setMediaSessionPlaybackState('paused');
		});

		audio.addEventListener('timeupdate', () => {
			this.playPosition = this.audio.currentTime;
		});

		audio.addEventListener('progress', () => {
			const audio = this.audio;
			for (let i = 0; i < audio.buffered.length; i++) {
				if (audio.buffered.start(audio.buffered.length - 1 - i) < audio.currentTime) {
					this.bufferedTo = audio.buffered.end(audio.buffered.length - 1 - i);
					break;
				}
			}
		});

		audio.addEventListener('waiting', _event => {
			console.log(audio.dataset.channel, 'onwaiting');
			this.playStatus = PlayStatus.Waiting;
		});

		audio.addEventListener('seeked', () => {
			console.log(audio.dataset.channel, 'onseeked');
		});

		audio.addEventListener('playing', () => {
			if (!this.currentSong) {
				return;
			}

			console.log(audio.dataset.channel, 'onplaying');

			this.playStatus = PlayStatus.Playing;
			void requestWakeLock();
			setMediaSessionPlaybackState('playing');
			this.updatePositionState();
		});

		return audio;
	}

	_ensureSupportedFormat() {
		if (this.extension) {
			return;
		}

		for (const {extension, mediaType, cutoff} of this.album.formats) {
			const canPlay = Boolean(this.audio.canPlayType?.(mediaType).replace(/no/, ''));
			if (canPlay) {
				this.extension = extension;
				this.frequencyCutoff = cutoff;
				break;
			}
		}

		if (this.extension) {
			console.log(`Found ${this.extension} support`);
		} else {
			console.error('No supported format for this browser');
		}
	}

	_ensureContext() {
		// Should be called after user actions (like click), because modern browsers
		// consider context creation as a deprecated autoplay operation
		// https://developer.mozilla.org/en-US/docs/Web/Media/Autoplay_guide#autoplay_of_media_elements
		if (!this.audioContext) {
			this.audioContext = new AudioContext();
			this.sourceAudio = this.audioContext.createMediaElementSource(this.audio);
			this.sourceNextAudio = this.audioContext.createMediaElementSource(this.nextAudio);
		}
	}

	_initAnalyser() {
		if (this.analyser) {
			return;
		}

		this._ensureContext();

		const context = this.audioContext;
		this.analyser = context.createAnalyser();
		this.analyser.fftSize = 1024;
		this.analyser.smoothingTimeConstant = 0.8;
		const bufferLength = this.analyser.frequencyBinCount;
		const binSize = context.sampleRate / this.analyser.fftSize;

		// Bark scale conversions
		const barkZ = (f: number) => 8.96 * Math.log(0.978 + 5 * Math.log(0.994 + ((f + 75.4) / 2173) ** 1.347));
		const barkF = (z: number) => 2173 * (Math.exp((Math.exp(z / 8.96) - 0.978) / 5) - 0.994) ** (1 / 1.347) - 75.4;

		// Because bins are not evenly distibuted, we'll perform a simple linear interpolation
		// to smooth lines in lower frequencies
		const freqToBins = (f: number) => {
			const x = f / binSize;
			if (Number.isInteger(x)) {
				return (array: Uint8Array) => array[x];
			}

			const x1 = Math.floor(x);
			const x2 = Math.ceil(x);
			return (array: Uint8Array) => array[x1] * (x2 - x) + array[x2] * (x - x1);
		};

		this.barCount = 120;
		// Expected sample rate is 44100, but dut to Nyquist–Shannon sampling theorem
		// FFT analyser spits out half of values to map to bars
		// ^ this is correct for flac, but there is a frequency cutoff for other formats
		const cutoff = this.frequencyCutoff;
		const sampleRateZ = barkZ(cutoff);
		const barToFreq = (bar: number) => {
			const z = bar / this.barCount * sampleRateZ;
			return Math.max(barkF(z), 0);
		};

		this.barMapping = [];
		for (let i = 0; i < this.barCount; i++) {
			this.barMapping.push(freqToBins(barToFreq(i)));
		}

		this.analyserBuffer = new Uint8Array(bufferLength);
	}

	analyze() {
		this.analyser.getByteFrequencyData(this.analyserBuffer);
		const out = new Float32Array(this.barCount);

		for (let i = 0; i < this.barCount; i++) {
			out[i] = this.barMapping[i](this.analyserBuffer) / 255;
		}

		return out;
	}

	// Shortcut accessors for stores
	get index() {
		return get(activeIndexStore);
	}

	set index(value: number) {
		activeIndexStore.set(value);
	}

	get playPosition() {
		return get(playPositionStore);
	}

	set playPosition(value: number) {
		playPositionStore.set(value);
	}

	get bufferedTo() {
		return get(bufferedToStore);
	}

	set bufferedTo(value: number) {
		bufferedToStore.set(value);
	}

	get playlist() {
		return get(albumStore).tracks;
	}

	get album() {
		return get(albumStore);
	}

	get playStatus() {
		return get(playStatusStore);
	}

	set playStatus(value: PlayStatus) {
		playStatusStore.set(value);
	}

	get volume() {
		return get(volumeStore);
	}

	get repeatTrack() {
		return get(repeatTrackStore);
	}

	get nonInstVisu() {
		return get(nonInstVisuStore);
	}

	get autoplay() {
		return get(autoplayStore);
	}

	get currentSong() {
		return this.playlist[this.index];
	}

	updatePositionState() {
		setMediaSessionPositionState({
			duration: this.currentSong.duration,
			playbackRate: this.audio.playbackRate,
			position: this.audio.currentTime,
		});
	}

	updateMetadata() {
		const album = this.album;
		const title = formatTrackTitle(album, this.currentSong);
		setMediaSessionMetadata({
			title,
			artist: album.artist,
			album: album.title,
			artwork: [
				{src: album.image, sizes: '1000x1000', type: 'image/png'},
			],
		});
	}

	prefetch(index: number) {
		if (index === null) {
			console.log('nothing to prefetch!!');
			return;
		}

		this._ensureSupportedFormat();

		const mirror = this.album.mirrors[0];
		const audioFile = mirror + '/' + this.album.tracks[index].audioBasename + '.' + this.extension;

		if (this.nextAudio.src !== audioFile) {
			console.log(`Fetch audio from ${audioFile} into ${this.nextAudio.dataset.channel}`);
			this.nextAudio.src = audioFile;
		}
	}

	loadTrack(index: number) {
		if (index === null) {
			console.log('nothing to load!!');
			return;
		}

		this._ensureSupportedFormat();

		this.index = index;

		const mirror = this.album.mirrors[0];
		const audioFile = mirror + '/' + this.currentSong.audioBasename + '.' + this.extension;

		if (this.nextAudio.src !== audioFile) {
			if (this.nextAudio.src) {
				console.log(`Using non-prefetched file, prefetched file was ${this.nextAudio.src}, but needed ${audioFile}`);
			}

			this.prefetch(index);
		}

		// Swap active audio with prefetched one
		[this.nextAudio, this.audio] = [this.audio, this.nextAudio];
		[this.sourceNextAudio, this.sourceAudio] = [this.sourceAudio, this.sourceNextAudio];

		this.nextAudio.src = '';

		this.bufferedTo = 0;
		this.updateMetadata();
	}

	play() {
		const instrumental = Boolean(this.currentSong.instrumental);

		this._ensureContext();

		if (instrumental || this.nonInstVisu) {
			console.log(this.audio.dataset.channel, 'Connect analyzer');
			this._initAnalyser();
			this.sourceAudio.connect(this.analyser).connect(this.audioContext.destination);
		} else {
			console.log(this.audio.dataset.channel, 'Disconnect analyzer');
			this.analyser?.disconnect();
			this.sourceAudio.connect(this.audioContext.destination);
		}

		this.bypassAutoplayRestriction();
		void this.audio.play();
	}

	bypassAutoplayRestriction() {
		if (!this.hasPlayed) {
			console.log('bypassAutoplayRestriction');

			this.audio.play().catch(() => {/* pass */});
			this.audio.pause();
			this.nextAudio.play().catch(() => {/* pass */});
			this.nextAudio.pause();
			this.hasPlayed = true;
		}
	}

	pause() {
		this.audio.pause();
		void releaseWakeLock();
	}

	stop() {
		this.audio.pause();
		this.audio.currentTime = 0;
		this.playStatus = PlayStatus.Stopped;
		void releaseWakeLock();

		console.log(this.audio.dataset.channel, 'stopped');
	}

	skip(direction: SkipDirection) {
		this.bypassAutoplayRestriction();

		let index = 0;
		if (direction === SkipDirection.Previous) {
			index = this.index - 1;
			if (index < 0) {
				index = this.playlist.length - 1;
			}
		} else {
			index = this.index + 1;
			if (index >= this.playlist.length) {
				index = 0;
			}
		}

		void goto(this.playlist[index].url);
	}

	seek(seekTime: number, fastSeek = false) {
		const audio = this.audio;
		console.log(audio.dataset.channel, `seek ${seekTime} / ${this.audio.duration}`);
		seekTime = Math.max(0, seekTime);

		if (fastSeek && 'fastSeek' in this.audio) {
			audio.fastSeek(seekTime);
			this.play();
			return;
		}

		audio.currentTime = seekTime;
		this.playPosition = seekTime;
		console.log(audio.dataset.channel, 'after seek', audio.currentTime);

		if (audio.currentTime === 0 && seekTime !== 0) {
			console.log('!!! <seeking failed, should never happen!> !!!');
		}
	}
}

declare global {
	interface Window {
		player: Player;
	}
}

export const player: Player = browser ? window.player || new Player() : null;

if (browser) {
	window.player = player;
}

type PlaybackState = 'none' | 'paused' | 'playing';
let setMediaSessionMetadata = (_metadata: MediaMetadataInit) => {/* empty */};
let setMediaSessionPlaybackState = (_state: PlaybackState) => {/* empty */};
let setMediaSessionPositionState = (_state: MediaPositionState) => {/* empty */};

if (browser && 'mediaSession' in navigator) {
	const mediaSession = navigator.mediaSession;

	setMediaSessionMetadata = (metadata: MediaMetadataInit) => {
		mediaSession.metadata = new MediaMetadata(metadata);
	};

	setMediaSessionPlaybackState = (state: PlaybackState) => {
		mediaSession.playbackState = state;
	};

	if ('setPositionState' in mediaSession) {
		setMediaSessionPositionState = (state: MediaPositionState) => {
			mediaSession.setPositionState(state);
		};
	}

	const actions: {[action in MediaSessionAction]?: MediaSessionActionHandler} = {
		play: () => {
			player.play();
		},
		pause: () => {
			player.pause();
		},
		stop: () => {
			player.stop();
		},
		seekbackward: ({seekOffset}) => {
			player.seek(player.playPosition - (seekOffset || 5));
		},
		seekforward: ({seekOffset}) => {
			player.seek(player.playPosition + (seekOffset || 5));
		},
		seekto: details => {
			player.seek(details.seekTime, details.fastSeek);
		},
		previoustrack: () => {
			player.skip(SkipDirection.Previous);
		},
		nexttrack: () => {
			player.skip(SkipDirection.Next);
		},
	};

	for (const [action, handler] of Object.entries(actions)) {
		try {
			mediaSession.setActionHandler(action as MediaSessionAction, handler);
		} catch {
			console.warn(`The media session action ${action} is not supported:`);
		}
	}
}
