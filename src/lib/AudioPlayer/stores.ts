import type {Readable} from 'svelte/store';
import {derived, writable, readable} from 'svelte/store';
import type {AlbumMetadata} from './types';
import {browser} from '$app/env';

export const album = writable(null as AlbumMetadata);
export const activeIndex = writable(null as number);

export const activeTrack = derived(
	[album, activeIndex],
	([$album, $activeIndex]) => {
		if ($album === null || $activeIndex === null) {
			return null;
		}

		return $album.tracks[$activeIndex];
	},
);

export enum PlayStatus {
	None, Stopped, Playing, Waiting, Paused, Ended,
}
export const playStatus = writable(PlayStatus.None);

export const playPosition = writable(0);
export const bufferedTo = writable(0);

export const duration: Readable<number> = derived(
	activeTrack,
	$activeTrack => $activeTrack?.duration,
);

function createVolumeStore() {
	const storedVolume = browser && localStorage.getItem('ap_volume') !== null
		? Number(localStorage.getItem('ap_volume')) : 1;

	const {subscribe, set} = writable(storedVolume);

	if (browser) {
		subscribe(value => {
			localStorage.setItem('ap_volume', value.toString());
		});
	}

	return {
		subscribe,
		set: (value: number) => {
			set(Math.max(0, Math.min(1, value)));
		},
	};
}

export const volume = createVolumeStore();

export const autoplay = writable(true);

export const instrumentalModeImage = writable(null as string);

export const showPlaylist = writable(
	!browser
    || (browser && window.matchMedia('(min-width: 768px)').matches),
);

export const showInfo = writable(false);

export const nonInstVisu = readable(!browser
	|| !window.matchMedia('(prefers-reduced-motion: reduce)').matches,
);

export const repeatTrack = writable(false);
