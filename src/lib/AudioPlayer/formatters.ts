import type {AlbumMetadata, TrackMetadata, TrackPageMetadata} from './types';

export const formatAlbumPageTitle = (albumMetadata: AlbumMetadata) => {
	const replacements: Record<string, string> = {
		'%album_name%': albumMetadata.title, // eslint-disable-line @typescript-eslint/naming-convention
		'%artist%': albumMetadata.artist,
	};
	const formatter = albumMetadata.formatters.albumPageTitle;
	const x = formatter.replace(/%\w+%/g, all => replacements[all] ?? all);
	return x;
};

export const formatInfoTitle = (
	albumMetadata: AlbumMetadata,
	trackPageMetadata: TrackPageMetadata,
) => {
	const replacements: Record<string, string> = {
		'%track_title%': trackPageMetadata.title, // eslint-disable-line @typescript-eslint/naming-convention
		'%track_subtitle%': trackPageMetadata.subtitle, // eslint-disable-line @typescript-eslint/naming-convention
		'%album_name%': albumMetadata.title, // eslint-disable-line @typescript-eslint/naming-convention
		'%artist%': albumMetadata.artist,
	};
	const formatter = albumMetadata.formatters.infoTitle;
	return formatter.replace(/%\w+%/g, all => replacements[all] ?? all);
};

export const formatTrackPageTitle = (
	albumMetadata: AlbumMetadata,
	trackPageMetadata: TrackPageMetadata,
) => {
	const replacements: Record<string, string> = {
		'%track_title%': trackPageMetadata.title, // eslint-disable-line @typescript-eslint/naming-convention
		'%album_name%': albumMetadata.title, // eslint-disable-line @typescript-eslint/naming-convention
		'%artist%': albumMetadata.artist,
	};
	const formatter = albumMetadata.formatters.trackPageTitle;
	return formatter.replace(/%\w+%/g, all => replacements[all] ?? all);
};

export const formatTrackTitle = (
	albumMetadata: AlbumMetadata,
	trackPageMetadata: TrackPageMetadata | TrackMetadata,
) => {
	if (!trackPageMetadata.subtitle) {
		return trackPageMetadata.title;
	}

	const replacements: Record<string, string> = {
		'%track_title%': trackPageMetadata.title, // eslint-disable-line @typescript-eslint/naming-convention
		'%track_subtitle%': trackPageMetadata.subtitle, // eslint-disable-line @typescript-eslint/naming-convention
	};
	const formatter = albumMetadata.formatters.trackTitle;
	return formatter.replace(/%\w+%/g, all => replacements[all] ?? all);
};
