import IntervalTree from '@flatten-js/interval-tree';
import type {TrackPage} from './types';
import {browser} from '$app/env';

export const buildIntervalTree = function (contentPage: TrackPage) {
	let intervalTree = null as IntervalTree<number>;

	if (browser) {
		if (contentPage.timings) {
			intervalTree = new IntervalTree();
			for (let i = 0; i < contentPage.timings.length; i++) {
				intervalTree.insert(contentPage.timings[i], i);
			}
		} else {
			intervalTree = null;
		}
	}

	return intervalTree;
};
