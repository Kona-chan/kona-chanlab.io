import yaml from 'js-yaml';
import {readSync} from 'to-vfile';
import {unified} from 'unified';
import remarkParse from 'remark-parse';
import frontmatter from 'remark-frontmatter';
import remark2rehype from 'remark-rehype';
import rehypeStringify from 'rehype-stringify';
import rehypeRaw from 'rehype-raw';
import type {AlbumIndexYaml, IndexPageMetadata} from './types';

export class IndexFileNotFound extends Error {}

export const loadIndexFile = (album: string) => {
	try {
		const filename = `content/listen/${album}/index.yaml`;
		const file = readSync(filename, 'utf8');
		const yamlContent = yaml.load(file.toString()) as AlbumIndexYaml;
		return yamlContent;
	} catch (error: unknown) {
		if ((error as {code: string}).code === 'ENOENT') {
			throw new IndexFileNotFound();
		}

		throw error;
	}
};

const remarkParser = unified()
	.use(remarkParse) // Parse Markdown to mdast syntax tree
	.use(frontmatter, ['yaml']); // Split front matter block

const loadMarkdownMetadata = function (album: string, language: string, track: string) {
	const filename = `content/listen/${album}/${language}/${track}.md`;
	const file = readSync(filename, 'utf8');
	const tree = remarkParser.parse(file);
	if (tree.children.length === 0 || tree.children[0].type !== 'yaml') {
		throw new Error('invalid markdown file');
	}

	const metadata = yaml.load(tree.children[0].value) as Record<string, unknown>;
	return metadata;
};

export const loadIndexMdMetadata = function (album: string, language: string) {
	return loadMarkdownMetadata(album, language, 'index') as IndexPageMetadata;
};

type TrackMdMetadata = {
	title: string;
	subtitle?: string;
	image?: string;
	translation?: string;
	translator?: string;
	translators?: string;
};

export const loadTrackMdMetadata = function (album: string, language: string, track: string) {
	return loadMarkdownMetadata(album, language, track) as TrackMdMetadata;
};

const runner = unified()
	.use(remark2rehype, {allowDangerousHtml: true}) // Mutate to rehype
	.use(rehypeRaw) // Deal with HTML in Markdown
	.use(rehypeStringify); // Stringify hast syntax tree to HTML

export const loadIndexMd = async (album: string, language: string) => {
	// Parse yaml from markdown file
	const indexFilename = `content/listen/${album}/${language}/index.md`;
	const indexFile = readSync(indexFilename, 'utf8');

	// Split markdown file to yaml and content parts
	const tree = remarkParser.parse(indexFile);

	if (tree.children.length === 0 || tree.children[0].type !== 'yaml') {
		throw new Error('invalid markdown file');
	}

	const metadata = yaml.load(tree.children[0].value) as IndexPageMetadata;
	tree.children.shift();

	// Generate html from markdown
	const content: string = runner.stringify(await runner.run(tree as any));

	return {metadata, content};
};
