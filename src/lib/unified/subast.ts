import type {Parent as UnistParent, Literal as Node} from 'unist';

export interface Parent extends UnistParent {
	children: Content[];
}
export type Content =
    | MetaContent
    | StyleContent
    | CaptionContent;

export type MetaContent = Meta;

export type StyleContent = Style;

export type CaptionContent = Caption;

export interface Root extends Parent {
	type: 'root';
}

export interface Meta extends Node {
	type: 'meta';
}

export interface Style extends Node {
	type: 'style';
}

export interface Caption extends Node {
	type: 'caption';
	start: number;
	end: number;
	duration: number;
	content: string;
	text: string;
}
