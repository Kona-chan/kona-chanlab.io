
import type {Plugin} from 'unified';
import {visit} from 'unist-util-visit';
import type {Root, Element} from 'hast';
import {hasProperty as has} from 'hast-util-has-property';

export const rehypeFixUrls: Plugin = () => (tree: Root) => {
	visit(tree, 'element', (node: Element) => {
		if (has(node, 'href') && typeof node.properties.href === 'string' && /^http:\/\/.+\.(?:zip|rar|mp3|dll|opus|flac)$/.test(node.properties.href)) {
			node.properties.href = 'https://vk.com/away.php?to=' + encodeURI(node.properties.href);
			// Opening such links may be less annoying if done in a new tab
			node.properties.target = '_blank';
			// Fix a security concern with offsite links
			// See: https://web.dev/external-anchors-use-rel-noopener/
			node.properties.rel = 'noopener';
		}
	});
};
