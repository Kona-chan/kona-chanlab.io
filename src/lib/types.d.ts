export type MarkdownContentPageMetadata = {
	title: string;
	description: string;
	image?: string;
	twitterTitle?: string;
	twitterDescription?: string;
	schema?: Record<string, any>;
};

export type MarkdownContentPage = {
	metadata: MarkdownContentPageMetadata;
	content: string;
};
