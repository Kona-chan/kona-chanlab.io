import {readSync} from 'to-vfile';
import {unified} from 'unified';
import remarkParse from 'remark-parse';
import gfm from 'remark-gfm';
import remark2rehype from 'remark-rehype';
import rehypeStringify from 'rehype-stringify';
import rehypeExternalLinks from 'rehype-external-links';
import rehypeRaw from 'rehype-raw';
import frontmatter from 'remark-frontmatter';
import slug from 'remark-slug';
import yaml from 'js-yaml';
import type {Root} from 'hast';
import Typograf from 'typograf';

// Does not work with modern browsers, disabled
// import {rehypeFixUrls} from '$lib/unified/rehype-fix-urls';

const typografs: Record<string, typograf.Typograf> = {
	ru: new Typograf({locale: ['ru']}),
	en: new Typograf({locale: ['en-US']}),
};

// Fill list of rules: https://github.com/typograf/typograf/blob/dev/docs/RULES.ru.md

for (const typograf of Object.values(typografs)) {
	// White quotes around links look bad
	typograf.disableRule('common/punctuation/quoteLink');
	// Disabled for 0x12345 (e. g. wallets)
	typograf.disableRule('common/number/times');
	// Disabled for PayPal
	typograf.disableRule('common/html/e-mail');
}

export class PageNotFound extends Error {}

const parser = unified()
	.use(remarkParse)
	.use(gfm)
	.use(frontmatter, ['yaml']);

const runner = unified()
	.use(slug)
	.use(remark2rehype, {allowDangerousHtml: true})
	.use(rehypeRaw)
	// Does not work with modern browsers, disabled
	// .use(rehypeFixUrls)
	.use(rehypeExternalLinks, {target: '_blank', rel: ['noopener', 'noreferrer']})
	.use(rehypeStringify);

export async function process(filename: string, language = 'ru') {
	const typograf = typografs[language];

	let file: ReturnType<typeof readSync>;
	try {
		file = readSync(filename, 'utf8');
	} catch (error: unknown) {
		throw (error as {code: string}).code === 'ENOENT' ? new PageNotFound() : error;
	}

	const tree = parser.parse(file);

	let metadata: Record<string, any> = null;
	if (tree.children.length > 0 && tree.children[0].type === 'yaml') {
		metadata = yaml.load(tree.children[0].value);
		tree.children = tree.children.slice(1, tree.children.length);
	}

	let content = runner.stringify(await runner.run(tree as Root));

	if (typograf) {
		content = typograf.execute(content);
	}

	return {metadata, content};
}
