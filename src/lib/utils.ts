import type {MarkdownContentPage} from './types';

export const loadContentPage = async function (page_name: string, fetch: (url: string) => Promise<Response>) {
	const response = await fetch(`/${page_name}.json`);
	if (response.ok) {
		const page = await response.json() as MarkdownContentPage;
		return {props: {page}};
	}
};
