// Run with:
// npm install ts-node
// node --no-warnings --experimental-specifier-resolution=node --loader ts-node/esm src/lib/translate-md-subtitles.ts

import {open, readdir} from 'node:fs/promises';
import path from 'node:path';
import {unified} from 'unified';
import {read} from 'to-vfile';
import {is} from 'unist-util-is';
import subsrtParse from './unified/subsrt-parse';
import type {Caption, Root as SubsrtRoot} from './unified/subast';

const sourceLang = 'ru';
const targetLang = 'ko';
const album = '2032';

const sourceDir = path.join('content', 'listen', album, sourceLang);
const targetDir = path.join('content', 'listen', album, targetLang);

const subsrtParser = unified()
	.use(subsrtParse, {format: 'ass'});

const readCaptions = async (filepath: string) => {
	const subtitlesFile = await read(filepath, 'utf8');
	const subtitles = subsrtParser.parse(subtitlesFile.value) as SubsrtRoot;
	const captions = subtitles.children
		.filter(node => is<Caption>(node, 'caption'))
		.map(({text}: Caption) => text);
	return captions;
};

const readMarkdown = async (filepath: string) => {
	const file = await read(filepath, 'utf8');
	return file.value.toString();
};

const cleanupLine = (line: string) => line.replace('<', '&lt;');

const processFile = async (filename: string) => {
	const sourceFilename = path.join(sourceDir, filename);
	const targetFilename = path.join(targetDir, filename);

	const sourceCaptions = await readCaptions(sourceFilename);
	const targetCaptions = await readCaptions(targetFilename);

	if (sourceCaptions.length !== targetCaptions.length) {
		console.log(`Lengths mismatch; file "${sourceFilename}" skipped`);
		return;
	}

	const mdFilename = filename.split('.')[0] + '.md';
	const sourceMdFilename = path.join(sourceDir, mdFilename);
	const targetMdFilename = path.join(targetDir, mdFilename);

	const sourceMd = await readMarkdown(sourceMdFilename);
	let targetMd = await readMarkdown(targetMdFilename);

	let sourcePos = 0;
	let targetPos = 0;
	for (const [i, sourceCaption] of sourceCaptions.entries()) {
		const sourceLine = cleanupLine(sourceCaption);
		const targetLine = cleanupLine(targetCaptions[i]);

		sourcePos = sourceMd.indexOf(sourceLine, sourcePos);
		if (sourcePos === -1) {
			throw new Error(`Unable to find line "${sourceLine}" in "${sourceMdFilename}"`);
		}

		targetPos = targetMd.indexOf(sourceLine, targetPos);
		if (targetPos === -1) {
			// Try to find and skip already translated line
			const replacedPos = targetMd.indexOf(targetLine, targetPos);
			if (replacedPos !== -1) {
				sourcePos += sourceLine.length;
				targetPos = replacedPos + targetLine.length;
				continue;
			}

			throw new Error(`Unable to find line "${sourceLine}" in "${targetMdFilename}"`);
		}

		targetMd = targetMd.slice(0, targetPos) + targetLine + targetMd.slice(targetPos + sourceLine.length);

		sourcePos += sourceLine.length;
		targetPos += targetLine.length;
	}

	const filehandle = await open(targetMdFilename, 'w');
	await filehandle.writeFile(targetMd);
};

(async () => {
	const processed: Array<Promise<void>> = [];
	const files = await readdir(sourceDir);
	for (const file of files) {
		if (file.endsWith('.ass')) {
			processed.push(processFile(file));
		}
	}

	try {
		await Promise.all(processed);
	} catch (error: unknown) {
		console.error((error as Error).message);
	}
})();
