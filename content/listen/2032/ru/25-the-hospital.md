---
title: Стационар
byArtist:
- name: Сергей Гончаров
  role: Заведующий отделением
- name: Ariel
  role: АСГУ
  image: /images/photos/ari_1.jpg
- name: Виталий Трофименко
  role: Анатолий Милиневский
  image: /images/photos/vit_2.jpg
- name: Виктор Аргонов
  role: Музыка, либретто, звук, аранжировка
  image: /images/photos/vic_1.jpg
---

**<u>[Кабинет заведующего отделением стационара]</u>**

**Заведующий отделением.** Здравствуйте!
Честно говоря, нас совершенно не предупредили.
Я узнал только 5 минут назад...
Визит депутата - такая честь для нас!
А вы ещё так похожи на...

**Милиневский.** Я по неофициальному вопросу.

**Заведующий отделением.** Я... Я вас слушаю!

**Милиневский.** В вашем отделении лежит Лимаева Светлана Александровна?

**Заведующий отделением.** Да-да, есть такая.

**Милиневский.** Я бы хотел её увидеть.

**Заведующий отделением.** Вы знаете…
Она сейчас в палате интенсивной терапии.
Она без сознания.

**Милиневский.** Без сознания?!
Что произошло?

**Заведующий отделением.** Её состояние критическое.
Вообще, знаете, довольно странный случай.
Результат какого-то продолжительного стресса.
Нервное истощение.
Может быть, какие-то сильные переживания, несчастная любовь...
Впрочем, она об этом не рассказывает.
Родители - или не знают, или тоже молчат...

**Милиневский.** Я обязательно должен её увидеть!

**Заведующий отделением.** Знаете, это исключено.
Туда можно входить только медперсоналу, да и то...

**Милиневский.** У меня очень важное дело.
Я не могу о нём распространяться.
Я вообще не хотел ничего говорить, но вот бумага из ЦК.

**Заведующий отделением.** О господи...
Что ж там могло потребоваться...
Ладно, пойдёмте.

**<u>[Палата интенсивной терапии]</u>**

**Лимаева.** Это вы...

**Заведующий отделением.** Я оставлю вас.
Тут всё по приборам видно.

**Лимаева.** Я знала, что вы придёте...
Только теперь уже, наверное, поздно?

**Милиневский.** Поздно?

**Лимаева.** Ну, я же скоро умру.
Так... надо.

**Милиневский.** Что ты говоришь!

**Лимаева.** Но это же ничего?
Когда-нибудь, через сотни, через тысячи лет, люди смогут вернуть к жизни всех когда-либо живших, ведь так же?
И там мы сможем быть вместе.
Ведь коммунизм - это когда каждый человек будет любить каждого?
Ведь это правда?

**Милиневский.** Правда... Наверное, правда...
Но воскрешение... все эти псевдорелигиозные фёдоровские идеи...
Это невозможно!

**Лимаева.** Я знаю, что так должно быть!
Мы будем вместе там!
Ведь мы не можем быть вместе здесь, и поэтому... я должна...

**Заведующий отделением.** Извините, у нас проблемы!

*&lt;периодический импульсный сигнал переходит в непрерывный>*

**Милиневский.** Нет… Нет!!!

**Заведующий отделением.** Мы её теряем...
Наталья Гильмутдиновна, ещё раз!..

*&lt;звук приборов>*

**Заведующий отделением.** Не помогает...
Зовите Харитонова!

**Милиневский.** А заморозка??
У вас делают быструю заморозку?
Ещё не поздно?

**Заведующий отделением.** Пожалуй, ещё возможно, но...

**Милиневский.** ЦК выделит средства!

**Заведующий отделением.** Да, я вам очень благодарен...
Конечно, технология ещё не обкатана.
Никаких гарантий...

**<u>[Крыльцо стационара, дождь]</u>**

**Заведующий отделением.** В любом случае, не вините себя...
Это всё равно было безнадёжно, я просто не стал говорить.
Хотя, когда вы туда зашли, я уже, было, подумал, что она...
Ну что ж, благодаря вам теперь, по крайней мере, есть надежда.
Может быть, через много лет...
