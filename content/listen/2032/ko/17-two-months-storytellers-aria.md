---
title: 두 달
translation: http://transural.egloos.com/314589
translator: GRU Fields
byArtist:
- name: 옐레나 야코베츠
  role: 여성 나레이터
  image: /images/photos/len_2.jpg
- name: 빅토르 아르고노프
  role: 총제작자
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**나레이터**

크렘린의 벽에 둘러쌓인채, 2달이 지나고..<br>
권력의 복도에서 잠잠해진 싸움들..<br>
그때까지 속박된 변화의 바람,<br>
사람들에겐, 그것은 광풍으로 보이는 그 바람.

그녀는 아직 버티고 있어요, 지금도 그렇구요.<br>
더욱 결정을 내리기엔 어려워지는 지금.<br>
모두가 알듯, 아무도 그 결정을 내리진 못해요.<br>
통제의 효율을 떨어트리지 않기 위해서죠.

13년이 지나며, 그녀도 자라났어요.<br>
우리와 때떄로, 놀랍도록 매우 닮아져 있어.<br>
그녀도 아주 쉽게 사랑에 빠질수 있고.<br>
권한에 시기심을 느끼겠지요. 마치 인간들처럼..
</div>