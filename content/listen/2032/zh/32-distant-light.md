---
title: 遥远的光
subtitle: ASGU或利玛爱娃的声乐曲
image: /listen/2032/cover-clean.jpg
translation: https://www.bilibili.com/audio/au2051666
translator: Elizabeth
byArtist:
- name: Ariel
  role: ASGU/全国自动化控制系统／利玛爱娃
  image: /images/photos/ari_1.jpg
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---

