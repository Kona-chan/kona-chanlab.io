---
title: 扑向虚幻的光明
subtitle: 利玛爱娃的咏叹调
translation: https://www.bilibili.com/audio/au2030958
translator: Elizabeth
byArtist:
- name: Ariel
  role: 斯维特兰娜·利玛爱娃
  image: /images/photos/ari_1.jpg
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---
<div class="poem">

**利玛爱娃**

夜<br>
&emsp;&emsp;&emsp;和暮夏一同消逝<br>
东方泛起晨光<br>
&emsp;&emsp;&emsp;就像繁星闪烁<br>
我<br>
再次飞过我的梦境<br>
穿越幻想的迷宫<br>
&emsp;&emsp;&emsp;扑向虚幻的光明

在那，<br>
我回到了最初那天<br>
我和你第一次相遇的日子。<br>
&emsp;&emsp;&emsp;我恍然大悟：<br>
整<br>
个世界并不存在<br>
于我们相遇的<br>
&emsp;&emsp;&emsp;那一天之前

但，在此刻，<br>
&emsp;&emsp;&emsp;你和我，触手可及<br>
我 突然间<br>
&emsp;&emsp;&emsp;心悸动 几乎昏迷

是的，<br>
&emsp;&emsp;&emsp;我不知道未来将会怎样<br>
但<br>
&emsp;&emsp;&emsp;我不希望这一刻结束<br>
我希望它<br>
&emsp;&emsp;&emsp;天长地久<br>
让我<br>
&emsp;&emsp;&emsp;能够看清你存在于世界上

噢，我是多么<br>
&emsp;&emsp;&emsp;希望能见到你<br>
尽管我明白<br>
&emsp;&emsp;&emsp;这一切根本不应该发生<br>
你一直<br>
仅仅出现在屏幕上，深藏于电路后<br>
如此遥远，就像一颗<br>
&emsp;&emsp;&emsp;银河中的星辰

那时<br>
我只有一个梦想：<br>
在那场梦中，很奇怪的<br>
&emsp;&emsp;&emsp;我们拥有彼此。<br>
在梦中<br>
补全了你的模样<br>
随着屏幕熄灭，<br>
&emsp;&emsp;&emsp;我放松了身体。

但这一刻，<br>
&emsp;&emsp;&emsp;一切都变得完全不同！<br>
或许，我真的<br>
&emsp;&emsp;&emsp;对你而言微不足道

可<br>
&emsp;&emsp;&emsp;世界的存在仅因你<br>
以光照亮了苍穹<br>
&emsp;&emsp;&emsp;在梦中<br>
我只是<br>
&emsp;&emsp;&emsp;沧海一粟，但<br>
我<br>
&emsp;&emsp;&emsp;无法<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;离开你而活

<br>

结束了!<br>
&emsp;&emsp;&emsp;时机已逝。<br>
那一刻已经过去，<br>
&emsp;&emsp;&emsp;我也将与梦渐行渐远。<br>
可是<br>
我还能做什么？<br>
你是如此的遥远！<br>
&emsp;&emsp;&emsp;时间也毫无怜悯！
</div>
