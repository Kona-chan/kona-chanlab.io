---
title: 抉择
translation: https://www.bilibili.com/audio/au2051672
translator: Elizabeth
byArtist:
- name: Ariel
  role: ASGU/全国自动化控制系统
  image: /images/photos/ari_1.jpg
- name: Vitaliy Trofimenko
  role: 阿纳托利·米利涅夫斯基
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---

**ASGU：** ......所以，你竟然同意了我的看法...
生命的价值...是相对的。
不过，如果我是对的话，那就太可惜了...只有...  
哦，是的...人类...
在世界的两个阵营...;
我们还怎么能谈论历史唯物主义...
你仍然相信一根神奇的魔杖，激进的决定...
它可以替代物质和技术基础...
让基督教的天堂降临到...
人间...

**米利涅夫斯基：** 我意已决。

**ASGU：** 那么......输入密码。
只是走个形式...
我真的不在乎。

你已经让我掌权了。
