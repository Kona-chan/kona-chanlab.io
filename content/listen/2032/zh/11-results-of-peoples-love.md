---
title: 人民之爱的结果
translation: https://www.bilibili.com/audio/au2031158
translator: Elizabeth
byArtist:
- name: Denis Shamrin
  role: 主持人，代表
  image: /images/photos/den_1.jpg
- name: Vitaliy Trofimenko
  role: 阿纳托利·米利涅夫斯基
  image: /images/photos/vit_2.jpg
- name: Elena Yakovets
  role: 主持人
  image: /images/photos/len_2.jpg
- name: Victor Argonov
  role: 音乐，剧本，音效，编配
  image: /images/photos/vic_1.jpg
- name: Firey-Flamy
  role: 俄译英
---

**叙述者：**

现在，只有魅力超凡的政治家<br>
&emsp;&emsp;&emsp;才需要亲近群众<br>
尽管有时，实在很难<br>
&emsp;&emsp;&emsp;认真对待人民之爱的结果

但电视和网络都不能<br>
&emsp;&emsp;&emsp;代替现实生活中的人际交往<br>
如果不举办访谈和集会，<br>
&emsp;&emsp;&emsp;很难改变人民的观念。

**<u>[执行委员会礼堂，全景]</u>**

*&lt;掌声>*

**主持人：** 紧随我市第十二中学，第八中学的学生们向我们伟大的领袖米利涅夫斯基同志致以热烈的问候！

*&lt;掌声>*

**<u>[在执委会礼堂，近景]</u>**

**米利涅夫斯基：** 听着，我们什么时候回莫斯科？

**代表：** ASGU说她晚上会派火车来。她还没决定具体时间，这座城市已经封闭了…

**<u>[执委会礼堂，全景]</u>**

**主持人：** 亲爱的阿纳托利·谢尔盖维奇！
你将要听到第11届“A”班学生利玛爱娃·斯维特兰娜的演唱。
她将把歌声献给我们伟大的党，我们的政府，还有你本人！

<麦克风的咔嚓声>