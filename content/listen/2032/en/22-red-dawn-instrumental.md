---
title: Red Dawn
subtitle: instrumental
image: /listen/2032/cover-clean.jpg
byArtist:
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
- name: Aleksandr Asinksy
  role: Guitar parts
---
