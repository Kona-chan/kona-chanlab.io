---
title: The Doubts
subtitle: Milinevsky's aria
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-09-%E2%80%93-%D1%81%D0%BE%D0%BC%D0%BD%D0%B5%D0%BD%D0%B8%D1%8F-2032-track-09-%E2%80%93-doubts.html
translator: FireyFlamy
byArtist:
- name: Vitaliy Trofimenko
  role: Anatoly Milinevsky
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**Milinevsky**

I know we are right… I know that I am right…<br>
&emsp;&emsp;&emsp;But there's still a shadow of doubt.<br>
Today, we are strong as never before<br>
&emsp;&emsp;&emsp;We are stronger than we ever were, in all these past years.

And sincerely thrilled people<br>
&emsp;&emsp;&emsp;Live with a dream of a progressive social structure<br>
And they are happy with the destiny they have<br>
&emsp;&emsp;&emsp;And they do value their great dream

And the time seems to have changed<br>
&emsp;&emsp;&emsp;And the roar of punitive turbine is in the past<br>
But, once again, the almighty country<br>
&emsp;&emsp;&emsp;Is naively hailing its own leader

Don’t we live in propaganda,<br>
&emsp;&emsp;&emsp;And everything is going to collapse, once things are out of control,<br>
While we’re still moving forward<br>
&emsp;&emsp;&emsp;Only thanks to the accuracy of the machine’s approach?!...
</div>