---
title: 200 Minutes
subtitle: Narrator's aria
translation: https://lyricstranslate.com/ru/200-minut-iz-opery-2032-versiya-2020-200-%D0%BC%D0%B8%D0%BD%D1%83%D1%82-%D0%B8%D0%B7-%D0%BE%D0%BF%D0%B5%D1%80%D1%8B-2032-%D0%B2%D0%B5%D1%80%D1%81%D0%B8%D1%8F-2020-200-minu.html
translator: FireyFlamy
byArtist:
- name: Elena Yakovets
  role: Narrator
  image: /images/photos/len_2.jpg
- name: Aleksandr Asinksy
  role: Guitar parts
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---
<div class="poem">

**Narrator**

200 minutes of journey –<br>
&emsp;&emsp;&emsp;1100 kilometers will be left behind,<br>
And the day will dissolve<br>
&emsp;&emsp;&emsp;In fiery whirlwinds of sunset wind.<br>
Everything that was left inside of it<br>
&emsp;&emsp;&emsp;Will seem, from a distance, to be just a trivial story<br>
In the noise of other problems,<br>
&emsp;&emsp;&emsp;It’s just a usual formal curiosity...

&emsp;&emsp;&emsp;And yet it’s impossible for us to love<br>
Everyone, who waits for our love on the planet,<br>
&emsp;&emsp;&emsp;A hand is unlikely to touch another person’s hand,<br>
In this fairytale.

We aren’t given a chance<br>
&emsp;&emsp;&emsp;To feel the emotions of other people inside of ourselves<br>
Our own feelings are of most importance,<br>
&emsp;&emsp;&emsp;Everything else is just a matter of control,<br>
And there’s, obviously,<br>
&emsp;&emsp;&emsp;No less egoism than in a usual life,<br>
And yet progress couldn’t<br>
&emsp;&emsp;&emsp;Give us the way out of this typical problem.

&emsp;&emsp;&emsp;And it is a paradox for thousands of people,<br>
But the wonderful feeling only disturbs them,<br>
&emsp;&emsp;&emsp;In the realm of monads without windows and doors<br>
It simply vanishes.

&emsp;&emsp;&emsp;And, today, this dream is unrealistic,<br>
The dream of loving everyone on earth:<br>
&emsp;&emsp;&emsp;The eternal spring hasn’t come to this world<br>
In this fairy tale.
</div>