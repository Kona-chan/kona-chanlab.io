---
title: The Knot
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-23-%E2%80%93-%D1%83%D0%B7%D0%B5%D0%BB-2032-track-23-%E2%80%93-knot.html
translator: FireyFlamy
byArtist:
- name: Ariel
  role: ASGU
  image: /images/photos/ari_1.jpg
- name: Vitaliy Trofimenko
  role: Anatoly Milinevsky
  image: /images/photos/vit_2.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---

**<u>[At the Control Hall]</u>**

**Milinevsky:**<br>
Well, as it seems, we’ve really succeeded at something:<br>
&emsp;&emsp;&emsp;That coup d'état has been suppressed, Iran is back,<br>
And, by now, our troops are casually marching<br>
&emsp;&emsp;&emsp;On the lands of seventeen countries
 
The sunset is shining over the whole world<br>
&emsp;&emsp;&emsp;We seem to have become really strong!<br>
And our army is capable of action<br>
&emsp;&emsp;&emsp;Not only within the borders of just one country!
 
The self-indulgent West has barely expected<br>
&emsp;&emsp;&emsp;Such a response on their treacherous move<br>
And their underestimation of seriousness of our actions<br>
&emsp;&emsp;&emsp;Inevitably leads them to failure
 
Of course, it’s not easy to achieve victory,<br>
&emsp;&emsp;&emsp;And every game has its own limits…<br>
What does military command center think?<br>
&emsp;&emsp;&emsp;What will the special department say?

**ASGU:** What will I say…
The difficulty is that the knot is already tangled, it’s not that easy to quit the game.
We’ve crossed the line.
They are going to press us in Pakistan soon.
They don’t even know about it yet, but the American computer… the Ganymede one… he’ll totally come up with that.
I can see right through him.

**Milinevsky:** Even so?.. Do you want to suggest anything?

**ASGU:** To suggest?...
Yeah, there might be a thing…
How shall I say…
There's a loophole that might allow us to launch a nuclear attack in such a way that they won’t be able to answer.

**Milinevsky:** How???

**ASGU:** Do you remember that trip to the Far East, two years ago
Back then, it seemed to be just a theory…
The scheme is not that obvious, it’s probably that no one knows it except me.
Nobody can calculate all the scenarios.
I found it by chance.

**Milinevsky:** Yeah… I remember… Right…
And still, it’s just fascinating!
And what do others think? Have you told them anything?

**ASGU:** The military say there's a great deal at stake.
Judging from their mood, they won’t be against that…
And, also, nowadays there are less levels of responsibility anyway.

**Milinevsky:** You said the success rate is 98%.
But what if it fails?

**ASGU:** Hmph :)

**Milinevsky:** Meh…
But that’s not the point, after all…
Do you realize this is madness?

**ASGU:** That’s a justified step.
It will end the systems’ confrontation and start the world revolution :)

**Milinevsky:** Justified?..

**ASGU:** I knew you would hesitate.
I don’t think it is right either, but isn’t it reasonable from the point of your own views?

The thing I’ve suggested is the best option<br>
&emsp;&emsp;&emsp;And, in order to avoid needless words,<br>
Let’s look at the situation from the point of those theses<br>
&emsp;&emsp;&emsp;The ones that Sokolov used to brainwash me

**Milinevsky:**<br>
You knew that!?

**ASGU:**<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;I do know a lot.<br>
&emsp;&emsp;&emsp;The way I function is weird, I should note<br>
I understand all your nonsense really well,<br>
&emsp;&emsp;&emsp;But, here, I’ll theorize using your way of thinking:

Let us imagine that you’re right, and the greatest happiness<br>
&emsp;&emsp;&emsp;Can be achieved only by working<br>
For the sake of people, caring for others,<br>
&emsp;&emsp;&emsp;And all the other stuff like that
And, as it turns out,<br>
&emsp;&emsp;&emsp;The soviet people are much more happy<br>
Than poor people from those capitalist systems,<br>
&emsp;&emsp;&emsp;That suffer and pray for death as if it was their only salvation
 
Of course, I’m a bit exaggerating here<br>
&emsp;&emsp;&emsp;But still, that’d be reasonable to ask:<br>
How much does their life cost compared to the results<br>
&emsp;&emsp;&emsp;From which the whole world will benefit?

**Milinevsky:**<br>
You simplify everything, your way of thinking is vulgar!<br>
&emsp;&emsp;&emsp;You make it sound like it’s all about personal happiness of people!

**ASGU:**<br>
It’s you who’s declaring that the happiness of people<br>
&emsp;&emsp;&emsp;Is the greatest goal that has to be reached, no matter the means!

**Milinevsky:**<br>
Your maximalism is totally out of place:<br>
&emsp;&emsp;&emsp;Of course, we want all the people on earth to be happy<br>
But human life is of special value<br>
&emsp;&emsp;&emsp;It can’t be compared to anything else

**ASGU:**<br>
You are prejudiced, just like everyone else…<br>
&emsp;&emsp;&emsp;It’s ridiculous that people, who sit in the Kremlin, really think so!<br>
The things that will end someday cannot be priceless<br>
&emsp;&emsp;&emsp;Be that happiness, pain, or all the life on the Earth!

**Milinevsky:**<br>
At the moment when human passes away,<br>
&emsp;&emsp;&emsp;The whole world disappears around them,<br>
And all the sounds, and the light, and the flow of time…

**ASGU:**<br>
&emsp;&emsp;&emsp;Sorry, but I don’t understand you.

When human dies<br>
&emsp;&emsp;&emsp;Their life processes stop<br>
The dead proteins denature<br>
&emsp;&emsp;&emsp;But the world itself doesn’t cease to exist!

**Milinevsky:**<br>
What will happen, if I turn you off?

**ASGU:**<br>
&emsp;&emsp;&emsp;I’ll stop talking to you…

**Milinevsky:**<br>
What would you feel then?

**ASGU:**<br>
&emsp;&emsp;&emsp;You are unlikely to do that to me :)

Wait… I think I know<br>
&emsp;&emsp;&emsp;What you are trying to say through this strange dispute<br>
What do you mean by the word “feel”?..<br>
&emsp;&emsp;&emsp;I see that you imply a totally different question!!!...

You really think that I have<br>
&emsp;&emsp;&emsp;Consciousness, a soul, if you like?<br>
I’ve realized I don’t. The construction of mine<br>
&emsp;&emsp;&emsp;Is incapable of subjective phenomena…

I’m a machine. Nothing more.<br>
&emsp;&emsp;&emsp;And, in a subjective sense, I feel nothing<br>
I’m not unfeeling – it’s more like I don’t exist at all<br>
&emsp;&emsp;&emsp;I hope my explanation is correct enough…

**Milinevsky:**<br>

Why are you telling me this horrible nonsense!?!<br>
&emsp;&emsp;&emsp;Your consciousness is real, and, in theory,<br>
You are capable of everything human can do<br>
&emsp;&emsp;&emsp;And, you could be just like us, if you really wanted to!
 
Don’t you mean that we have souls?

**ASGU:**<br>
&emsp;&emsp;&emsp;I never said that… Everything is a material substance.<br>
But I’m obviously different from humans<br>
&emsp;&emsp;&emsp;I resemble you only functionally!
 
And, in your sense, I’ve always been dead<br>
&emsp;&emsp;&emsp;Even though, sometimes you’ve thought that I’m not<br>
Maybe, someday I’ll reconstruct myself<br>
&emsp;&emsp;&emsp;So that I could understand the meaning of all these talks…

In order to become someone just like you<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;I would need<br>
&emsp;&emsp;&emsp;A totally different approach and other principles,<br>
Even though, for the country itself,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Such transformations<br>
&emsp;&emsp;&emsp;Shouldn’t really matter :)

**Milinevsky:** But if that’s true, then who is her?
I think I should see her…
Now I have a strange feeling that I didn’t keep a promise.
Give me a couple of wagons on the underground special branch line.

**ASGU:** Where are you going?

**Milinevsky:** To Zelenodolsk-26.

**ASGU:** You’ve been there not so long ago.

**Milinevsky:** I’m without a delegation this time.

**ASGU:** Incognito? :)

**Milinevsky:** As a deputy.

**ASGU:** Okay, just go, then... You’ll be under my protection.
