---
title: The Congress
translation: https://lyricstranslate.com/ru/%C2%AB2032%C2%BB-%D1%82%D1%80%D0%B5%D0%BA-05-%E2%80%93-%D1%81%D1%8A%D0%B5%D0%B7%D0%B4-2032-track-05-%E2%80%93-congress.html
translator: FireyFlamy
byArtist:
- name: Vitaliy Trofimenko
  role: Anatoly Milinevsky
  image: /images/photos/vit_2.jpg
- name: Ariel
  role: ASGU
  image: /images/photos/ari_1.jpg
- name: Victor Argonov
  role: Music, libretto, sound, arrangement
  image: /images/photos/vic_1.jpg
---

**<u>[At the conference hall]</u>**

**ASGU:** I give the floor to the General Secretary of the Central Committee of the Soviet Union Communist Party Anatony Sergeevich Milinevsky.

**&lt;applause>**

**Milinevsky:** Dear deputies, dear Politburo members.
Today, on the 23rd of July 2032, we’re opening the congress timed to Nikolai Iosifovich Plotnikov’s memorial day, who was an outstanding statesman of the Soviet Union and the General Secretary of CC CPSU. And before we start discussing the agenda, I’d like to say a few words about the role of this person in our recent history.  
It’s no secret that our country was in a rather difficult economic situation in the mid 80’s of the last century…

**ASGU:**

Sometimes, it’s so interesting to observe<br>
&emsp;&emsp;&emsp;The expressions of some deputies :)<br>
&emsp;&emsp;&emsp;They will approve of my new course,<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Unanimously, with no debates<br>
&emsp;&emsp;&emsp;They don’t want to waste their time:<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Their vote is just a formality, it doesn’t matter that much.

**<u>[At the conference hall]</u>**

**Milinevsky:** Milinevsky: …the international situation wasn’t easy either. The bureaucratic layer didn’t fit the Marxist-Leninist ideas and turned out to be a hindrance to the development of our society. Thanks to the managerial talent of Grigory Vasilyevich Romanov, who was elected as the CC General Secretary back in 1985, the situation was partly improved, and our country restored its positions. But when Nikolai Plotnikov came into power, we followed a truly great path.  
A remarkable progress in terms of the governance theory and artificial intelligence, along with the direct involvement of academic Plotnikov, allowed to finish the construction of ASGU – the Universal Automated System of Governance – 13 years ago. At first it was being developed in the 1960s by the Kiev Institute of Cybernetics, and by its director Viktor Glushkov personally, for the sake of increasing the efficiency of the economic planning, but, after being connected to the State Information Network 4 years ago, it has fundamentally changed the whole structure of our governance as long as the lives of millions of Soviet workers. Today, you will also hear her report.  
We all know how much the state of things in the world have changed, too.  
Soviet Union’s territory was widely expanded thanks to brotherly people of Mongolia and North Afghanistan joining us. Another memorable event of the last decades was the creation of the Middle Eastern Socialistic Confederation, whereas their people put an end to the medieval times and chose the innovative way of development...

<br>

**ASGU:** Dear deputies, dear Politburo members. Now, according to the agenda, you will be presented a report by the Universal Automated System of Governance.  
In this report, I’d like to acquaint you…

**<u>[At Kremlin office]</u>**

**ASGU:**

*Why are you here? I’m making a report right now…*

**Milinevsky:**

&emsp;&emsp;&emsp;I know. I’ve already heard it.<br>
And I’ve told you, I don’t quite agree with it.<br>
&emsp;&emsp;&emsp;It’s too early to make such statements, I’m afraid.

**<u>[At the conference hall]</u>**

**ASGU:**

…with some provisions of my new development program for the next 30 years till 2062, which will be implemented at USSR and, as much as possible, at the other countries of the socialist bloc. Some of you have already had the chance to read about the so-called “theses” on a transition to the second scheme of manufacturing automation.
The transition of the main part of the production capacity to being controlled by me was already considered as the possible framework for partial abolition of commodity and money relations. However, it was opposed by certain conservative circles.
Nonetheless…


**<u>[At Kremlin office]</u>**

**Milinevsky:**<br>
The only things described in your program are technological advances.<br>
I see no opportunities for the moral growth of a person!<br>
**ASGU:**<br>
That’s so absurd to hear nowadays<br>
About this misconception of the age of enlightenment.<br>
You know that labor productivity<br>
Is a criteria of social development!<br>
**Milinevsky:**<br>
There’s a difference between a person that lives in order to work,<br>
And the one who just sits back and watches you do everything!

**ASGU:**<br>
I’m not going to deprive people<br>
Of things that they enjoy doing.<br>
**Milinevsky:**<br>
And how many of them will turn into deadbeats?<br>
**ASGU:**<br>
I don’t quite understand this word.
