---
title: 'Complex Numbers: музыка и видео в жанре научной фантастики'
description: 'Поём про восприятие реальности, крионику, трансгуманизм, метаэтику, и советских школьниц из будущего.'

image: 'https://complexnumbers.ru/images/logo.png'
twitterTitle: 'Complex Numbers: научная фантастика'
twitterDescription: 'Музыка и видео, оперы на русском языке. Философия, будущее, сознание, работа мозга, восприятие реальности.'

schema:
  '@context': 'http://schema.org'
  '@type': MusicGroup
  name: Complex Numbers
  alternateName: Комплексные Числа
  url: 'https://complexnumbers.ru/'
  logo: 'https://complexnumbers.ru/images/logo.png'
  sameAs:
    - 'https://www.youtube.com/channel/UCAYH-z9hCpzMZAEAHV6WpXA'
    - 'https://open.spotify.com/artist/3PBUz26WYPxZo1uFbS2RbV'
    - 'https://complexnumbers.bandcamp.com/'
    - 'https://music.apple.com/artist/complex-numbers/1140302618'
    - 'https://music.amazon.com/artists/B01JKKORCM'
    - 'https://www.last.fm/music/Complex+Numbers'
    - 'https://musicbrainz.org/artist/d2062464-9c2e-4d1f-8005-dac138da03e6'
    - 'https://www.discogs.com/artist/1439159'
    - 'https://vk.com/viktorargonovproject'
    - 'https://www.wikidata.org/wiki/Q100269171'
---

Complex Numbers (включая Виктор Аргонов Project) — российская музыкальная группа, работающая в разных форматах от отдельных треков до симфоний и опер. Основное наше кредо — сочетание доступной мелодичной электронной музыки (вплоть до намеренной эксплуатации поп-штампов) и сложных интеллектуальных смыслов научно-технического и философско-футурологического плана. В силу этого сочетания, на современной сцене проект занимает почти пустую нишу. Вся музыка группы и материалы сайта распространяются под лицензией [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode.ru). Смотрите также [Черновик статьи на Википедии](https://ru.wikipedia.org/wiki/Участник:Vitaly_Zdanevich/Complex_Numbers_(музыкальная_группа)), [Commons](https://commons.wikimedia.org/wiki/Category:Complex_Numbers_(musical_group)), [Wikidata](https://www.wikidata.org/wiki/Q100269171).

## Наградить группу

<div style="text-align: left;">

Карта (Сбер) 4276 1609 6787 1493 Виктор Юрьевич А, YooMoney 41001159842314, Qiwi 79024826592, PayPal argonov<span>@</span>list.ru\
Bitcoin 1QEGaVdZ7wAhe6uAd4ARVhbi81tK9XXgjT, Etherium 0x2f552b8db5e7b8237f70670a58c1f330e610ca8d

</div>

## Дискография и видеография

<div class="albumblock">
<div>
<img src="/images/covers/btta-blurred.jpg" class="albumcover-small" width="256px" />
</div>
<div class="albumdescription">

### Песни для нового альбома (2016–2022, в работе)&emsp;<span class="normal">[Скачать mp3 в zip](http://store.complexnumbers.lenin.ru/2022-between-the-two-ages-incomplete.zip)&emsp;[VK](https://vk.com/audios-23865151?section=playlists&z=audio_playlist-23865151_72220826)</span>

[Межкарантинное лето](http://commons.wikimedia.org/wiki/File:Summer-between-quarantines.flac)
&emsp;[Разбор смысла](https://vk.com/@viktorargonovproject-2011-11-18)\
[В дни чудес (Три бозона)](http://commons.wikimedia.org/wiki/File:In-the-days-of-wonder.flac) [1]
&emsp;[Разбор смысла](https://vk.com/@viktorargonovproject-complex-numbers-v-dni-chudes-tri-bozona-smysl-pesni)\
[Пролегла черта (Меж двух эпох)](https://commons.wikimedia.org/wiki/File:The-line-has-been-drawn.flac) [1]\
[Пробуждение (Никто не решал, кем родиться) [Версия 2020]](https://commons.wikimedia.org/wiki/File:Awakening-2020-version.flac)\
[200 минут (Плен инстинктов жесток) [Версия 2020]](http://commons.wikimedia.org/wiki/File:200-minutes-2020-version.flac) [3]\
[1000 лет назад (Мне звёзды казались так близко)](http://commons.wikimedia.org/wiki/File:1000-years-ago.flac)
&emsp;[История и обсуждение](https://argonov.livejournal.com/227741.html)\
[Сердцу прикажешь (К чувствам ключи)](https://commons.wikimedia.org/wiki/File:The-heart-will-obey-you.flac)
&emsp;[История и обсуждение](https://argonov.livejournal.com/227383.html)\
[Время придёт (Изучает прогресс поведение людей)](https://commons.wikimedia.org/wiki/File:The-time-will-come.flac) [2]
&emsp;[История и обсуждение](https://argonov.livejournal.com/204039.html)\
Музыка и текст: [Виктор Аргонов](https://www.wikidata.org/wiki/Q105755025).\
Вокал: [Len](https://www.wikidata.org/wiki/Q105906250), кроме: [1] [Oleviia](https://www.wikidata.org/wiki/Q106182851), [2] Len, [Ariel](https://www.wikidata.org/wiki/Q105905487), [3] [Елена Яковец](https://www.wikidata.org/wiki/Q105962641), Len

</div></div>

<div class="albumblock">
<a href="/good-fairy-tales/"><img src="/images/covers/good-tales.jpg" class="albumcover-small" width="256px" height="256px" /></a>
<div class="albumdescription">

### [Сказки добрые. Каверы-переосмысления (2003–2021)](/good-fairy-tales/)&emsp;<span class="normal">[Скачать mp3 в zip](http://store.complexnumbers.lenin.ru/2021-good-fairy-tales.zip)&emsp;[VK](https://vk.com/wall-23865151_34082)</span>

11 треков разных лет, в том числе со страницами обсуждений:  
Сказки добрые (Каролина Trance Cover)  [История и обсуждение](https://argonov.livejournal.com/202947.html)  
8 минут (Игорь Николаев Trance Cover)  [История и обсуждение](https://argonov.livejournal.com/228740.html)

Переработка музыки и текстов: Виктор Аргонов. Вокал: Ariel, Len, Oleviia

</div></div>


<div class="albumblock">  
<a href="/we-22nd-century/"><img src="/images/covers/we256-2.jpg" class="albumcover-small" width="256px" /></a>
<div class="albumdescription">

### [Мы, XXII век. Техно-опера (видео, 2017–2018)](/we-22nd-century/)

Анимационная экранизация одноимённой оперы. Серьёзно раскрывает смысл.

Видео, сопроводительный текст: Виктор Аргонов.

[Посмотреть на YouTube](https://www.youtube.com/watch?v=YrXk2buqsgg)   [Скачать mp3 и видео в zip](http://store.complexnumbers.lenin.ru/(2018)%20%d0%9c%d1%8b,%20XXII%20%d0%b2%d0%b5%d0%ba%20(%d1%82%d0%b5%d1%85%d0%bd%d0%be-%d0%be%d0%bf%d0%b5%d1%80%d0%b0).zip)   [Обсуждение](https://argonov.livejournal.com/223011.html)

</div></div>


<div class="albumblock">  
<a href="/we-22nd-century/"><img src="/images/covers/we402.jpg" class="albumcover-small" width="256px" /></a>
<div class="albumdescription">

### [Мы, XXII век. Техно-опера (аудио, 2016–2018)](/we-22nd-century/)

Рассказ о человеке XX века, попавшем в XXII век через криозаморозку. Новое общество считает себя раем. Но с позиции идеалов XX века в нём есть антиутопические черты включая размытие понятия личности. Причём столь глубокое и хитро обоснованное, что Замятин, Хаксли и Оруэлл курят в сторонке.

Музыка: Андрей Климковский, Виктор Аргонов. Текст, аранжировка: Виктор Аргонов. Вокал: Len, Ariel.

[Скачать mp3 в zip](http://store.complexnumbers.lenin.ru/(2018)%20%d0%9c%d1%8b,%20XXII%20%d0%b2%d0%b5%d0%ba%20(%d1%82%d0%b5%d1%85%d0%bd%d0%be-%d0%be%d0%bf%d0%b5%d1%80%d0%b0).zip)      
[Commons](https://commons.wikimedia.org/wiki/File:Complex_Numbers_-_%D0%9C%D1%8B,_XXII_%D0%B2%D0%B5%D0%BA._%D0%A2%D0%B5%D1%85%D0%BD%D0%BE-%D0%BE%D0%BF%D0%B5%D1%80%D0%B0_(%D1%84%D0%B8%D0%BD%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F_%D1%80%D1%83%D1%81%D1%81%D0%BA%D0%B0%D1%8F_%D0%B2%D0%B5%D1%80%D1%81%D0%B8%D1%8F_2018-10-05)-YrXk2buqsgg.webm)      
[VK](https://vk.com/audios-23865151?section=playlists&z=audio_playlist-23865151_83082486)      
[Обсуждение](https://argonov.livejournal.com/214695.html)

[Скачать вокал и фонограммы](https://gitlab.com/complexnumbers/we22-src/-/archive/master/we22-src-master.zip)

</div></div>


<div class="albumblock">
<div class="albumdescription">

### Каверы и ремиксы других музыкантов на нас &nbsp;&nbsp;&nbsp;&nbsp;<span class="normal">[VK](https://vk.com/audios-23865151?section=playlists&z=audio_playlist-23865151_64748401)&nbsp;&nbsp;&nbsp;&nbsp;[zip](http://store.complexnumbers.lenin.ru/%d0%9a%d0%b0%d0%b2%d0%b5%d1%80%d1%8b%20%d0%bd%d0%b0%20%d0%bd%d0%b0%d1%81%20(2015-2016).zip)&nbsp;&nbsp;&nbsp;&nbsp;[Commons](https://commons.wikimedia.org/wiki/Category:Complex_Numbers_(musical_group):_remixes_and_covers)</span>

Время придёт (Tycoos remix), Там, за чертой (DIP Project remix), Там, за чертой (Bananafox remix), Свой путь (Tycoos remix),  
Сделать шаг (Reaktivz remix), Там, за чертой (Sonar Spray cover), Предания неведомых миров (Isdays cover).

</div></div>


<div class="albumblock">  
<a href="/reth/"><img src="/images/covers/re402.jpg" class="albumcover-small" width="256px" /></a>
<div class="albumdescription">

### [Переосмысляя прогресс. Техно-симфония (видео, 2013–2015)](/reth/)

Видеосинопсис одноимённой симфонии. Необходим для понимания смысла.

Видео, сопроводительный текст: Виктор Аргонов.

[Посмотреть на YouTube](https://youtu.be/EpVd-TXDUk4)  [Скачать mp3 и видео в zip](http://store.complexnumbers.lenin.ru/(2015)%20%d0%9f%d0%b5%d1%80%d0%b5%d0%be%d1%81%d0%bc%d1%8b%d1%81%d0%bb%d1%8f%d1%8f%20%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b5%d1%81%d1%81%20(%d1%82%d0%b5%d1%85%d0%bd%d0%be-%d1%81%d0%b8%d0%bc%d1%84%d0%be%d0%bd%d0%b8%d1%8f).zip)

</div></div>


<div class="albumblock">  
<a href="/reth/"><img src="/images/covers/prog-402.jpg" class="albumcover-small" width="256px" /></a>

<div class="albumdescription">

### [Переосмысляя прогресс. Техно-симфония (аудио, 2013–2015)](/reth/)

Философский рассказ о прошлой и будущей истории отношений человека и технологий: восхищение "прогрессом ради прогресса", переосмысление идеалов, попытки бегства, новые открытия и реальное духовное преображение.

Музыка, текст, аранжировка: Виктор Аргонов. Звук: [Саян Андриянов](https://www.wikidata.org/wiki/Q108058755). Вокал: Елена Яковец, Len, Ariel.

[Скачать mp3 и видео в zip](http://store.complexnumbers.lenin.ru/(2015)%20%d0%9f%d0%b5%d1%80%d0%b5%d0%be%d1%81%d0%bc%d1%8b%d1%81%d0%bb%d1%8f%d1%8f%20%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b5%d1%81%d1%81%20(%d1%82%d0%b5%d1%85%d0%bd%d0%be-%d1%81%d0%b8%d0%bc%d1%84%d0%be%d0%bd%d0%b8%d1%8f).zip)  [Скачать вокал и фоногр.](https://gitlab.com/complexnumbers/reth-src/-/archive/master/reth-src-master.zip)

</div></div>

<div class="albumblock">  
<a href="/merm/"><img src="/images/covers/merm-402.jpg" class="albumcover-small" width="256px" /></a>
<div class="albumdescription">

### [Русалочка. Техно-опера (2005–2013)](/merm/)

Фантастическое переосмысление андерсеновского сюжета в условиях современной России.

Музыка, текст, аранжировка: Виктор Аргонов. Вокальные роли: Ariel, Александра Пряха, [Николай Перминов](https://www.wikidata.org/wiki/Q105943304), [Виталий Трофименко](https://www.wikidata.org/wiki/Q105943505). Речевые роли: [Александр Солнышкин](https://www.wikidata.org/wiki/Q105943935), [Виталий Зданевич](https://www.wikidata.org/wiki/Q105704549), Виктор Аргонов.

[Скачать mp3 в zip](http://store.complexnumbers.lenin.ru/(2013)%20%d0%a0%d1%83%d1%81%d0%b0%d0%bb%d0%be%d1%87%d0%ba%d0%b0%20(%d1%82%d0%b5%d1%85%d0%bd%d0%be-%d0%be%d0%bf%d0%b5%d1%80%d0%b0).zip)  [Скачать вокал и фонограммы](https://gitlab.com/complexnumbers/mermaid-src/-/archive/master/mermaid-src-master.zip?path=MERM)

</div></div>


<div class="albumblock">  
<a href="/2032/"><img src="/images/covers/2032-402.jpg" class="albumcover-small" width="256px" /></a>
<div class="albumdescription">

### [2032. Легенда о несбывшемся гряущем. Техно-опера (1999–2007)](/2032/)

Альтернативная история СССР 2032 года.

Музыка, текст, аранжировка: Виктор Аргонов. Гитарные партии: [Александр Асинский](https://www.wikidata.org/wiki/Q106489417). Вокальные роли: Виталий Трофименко, Ariel, Елена Яковец. Речевые роли: [Денис Шамрин](https://www.wikidata.org/wiki/Q106094199), [Евгений Гриванов](https://www.wikidata.org/wiki/Q109803279), [Андрей Иванов](https://www.wikidata.org/wiki/Q109803629), [Дмитрий Гончар](https://www.wikidata.org/wiki/Q106093180), [Сергей Гончаров](https://www.wikidata.org/wiki/Q109803729).

**[Слушать на сайте](/listen/2032/ru/)**  
[Скачать mp3 в zip](http://store.complexnumbers.lenin.ru/(2007)%202032.%20%d0%9b%d0%b5%d0%b3%d0%b5%d0%bd%d0%b4%d0%b0%20%d0%be%20%d0%bd%d0%b5%d1%81%d0%b1%d1%8b%d0%b2%d1%88%d0%b5%d0%bc%d1%81%d1%8f%20%d0%b3%d1%80%d1%8f%d0%b4%d1%83%d1%89%d0%b5%d0%bc%20(%d1%82%d0%b5%d1%85%d0%bd%d0%be-%d0%be%d0%bf%d0%b5%d1%80%d0%b0).zip)  [Скачать вокал и фонограммы](http://store.complexnumbers.lenin.ru/src_basic_2032.zip)

</div></div>

<div class="albumblock">  
<div class="albumdescription">

### Каверы на музыку игр nintendo, не вошедшие в альбомы (1996–2008)  <span class="normal">[VK](https://vk.com/audio?z=audio_playlist-23865151_76950283/8b72fafc583853b652) [zip](http://store.complexnumbers.lenin.ru/%d0%9d%d0%b0%d1%88%d0%b8%20%d0%ba%d0%b0%d0%b2%d0%b5%d1%80%d1%8b%20%d0%bd%d0%b0%20Nintendo%20(2003-2008).zip)</span>

Salvage Chute (feat Konami — Bucky O'Hare),\
Parting (feat Tecmo — Ninja Gaiden 3),\
Escape (feat. Konami — Bucky O'Hare),\
Again with You (feat Tecmo — Ninja Gaiden 3),\
Last Step to Victory (feat Technos — Double Dragon 2),\
2029 A. D. (feat Natsume — Shadow of the Ninja),\
Desert Forsage (feat Tecmo — Ninja Gaiden 3),\
Peaceful Sky (feat Konami — Super Contra),\
Ninja Tsoy (feat Tecmo — Ninja Gaiden 3; Кино; Сектор Газа).

</div></div>

<div class="albumblock">  
<img src="/images/covers/in402.jpg" class="albumcover-small" width="256px" />
<div class="albumdescription">

### [Неизбежность (видео, 2008–2009)](https://www.wikidata.org/wiki/Q108129875)

Музыкальное видео на одноимённую песню из альбома "Последнее кольцо". Первое видео группы. Существенно раскрывает смысл песни.

Музыка, сопроводительный текст: Виктор Аргонов.

[Посмотреть на YouTube](https://www.youtube.com/watch?v=FMJNta-okRw)   [Скачать mp3 и видео в zip](http://store.complexnumbers.lenin.ru/(2009)%20%d0%9f%d0%be%d1%81%d0%bb%d0%b5%d0%b4%d0%bd%d0%b5%d0%b5%20%d0%ba%d0%be%d0%bb%d1%8c%d1%86%d0%be.zip)

</div></div>


<div class="albumblock">  
<a href="last-ring"><img src="/images/covers/la402r.jpg" class="albumcover-small" width="256px" /></a>

<div class="albumdescription">

### [Последнее кольцо (2007–2009)](/last-ring/)

Третий альбом Complex Numbers.

Музыка: Виктор Аргонов, Андрей Климковский, Александр Асинский, Hydrogen. Текст: Виктор Аргонов, Ariel. Гитарные партии: Александр Асинский. Вокал: Ariel, [Наталья Митрофанова](https://www.wikidata.org/wiki/Q105942454).

[Скачать mp3 и видео в zip](http://store.complexnumbers.lenin.ru/(2009)%20%d0%9f%d0%be%d1%81%d0%bb%d0%b5%d0%b4%d0%bd%d0%b5%d0%b5%20%d0%ba%d0%be%d0%bb%d1%8c%d1%86%d0%be.zip)  [Скачать вокал и фонограммы](http://store.complexnumbers.lenin.ru/src_basic_last.zip)

[Слушать главную песню альбома на Commons](https://commons.wikimedia.org/wiki/File:Complex-numbers--last-ring.opus)

[Слушать в VK](https://vk.com/audios-23865151?section=playlists&z=audio_playlist-23865151_72225350)

</div></div>


<div class="albumblock">  
<a href="the-morning-of-the-new-millennium"><img src="/images/covers/mo402r.jpg" class="albumcover-small" width="256px" /></a>

<div class="albumdescription">  

### [Утро нового тысячелетия (1998–2003)](/the-morning-of-the-new-millennium/)

Второй альбом Complex Numbers.

Музыка: Виктор Аргонов, Александр Асинский, [Илья Пятов](https://www.wikidata.org/wiki/Q106314773), [Фрол Запольский](https://www.wikidata.org/wiki/Q102362017). Текст: Виктор Аргонов. Вокал: Наталья Митрофанова.

[Скачать mp3 в zip](http://store.complexnumbers.lenin.ru/(2003)%20%d0%a3%d1%82%d1%80%d0%be%20%d0%bd%d0%be%d0%b2%d0%be%d0%b3%d0%be%20%d1%82%d1%8b%d1%81%d1%8f%d1%87%d0%b5%d0%bb%d0%b5%d1%82%d0%b8%d1%8f.zip)  [Скачать вокал и фонограммы](http://store.complexnumbers.lenin.ru/src_basic_morn.zip)

[Слушать в VK](https://vk.com/audios-23865151?section=playlists&z=audio_playlist-23865151_72225328)

</div></div>


<div class="albumblock">  
<a href="/earths-attraction/"><img src="/images/covers/ea402r.jpg" class="albumcover-small" width="256px" /></a>

<div class="albumdescription">  

### [Земное притяжение (1996–2002)](/earths-attraction/)

Первый альбом Complex Numbers.

Музыка: Фрол Запольский, Виктор Аргонов, Илья Пятов, Александр Асинский. Текст: Виктор Аргонов, Илья Пятов. Вокал: Наталья Митрофанова.

[Скачать mp3 в zip](http://store.complexnumbers.lenin.ru/(2002)%20%d0%97%d0%b5%d0%bc%d0%bd%d0%be%d0%b5%20%d0%bf%d1%80%d0%b8%d1%82%d1%8f%d0%b6%d0%b5%d0%bd%d0%b8%d0%b5.zip)  [Скачать вокал и фонограммы](http://store.complexnumbers.lenin.ru/src_basic_eart.zip)

[Слушать в VK](https://vk.com/audios-23865151?section=playlists&z=audio_playlist-23865151_72225293)

</div></div>


<div style="text-align: right">

[Git репозиторий этого сайта](https://gitlab.com/complexnumbers/complexnumbers-ru)

</div>
