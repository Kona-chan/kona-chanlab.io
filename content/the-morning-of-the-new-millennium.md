---
title: 'Утро нового тысячелетия — Complex Numbers'
description: Первый альбом Complex Numbers.
image: 'http://complexnumbers.ru/images/covers/mo402r.jpg'
---

<div class="row">
<img src="/images/covers/mo402r.jpg" class="albumcover-small" width="256px" />

<div class="albumdescription">

### Утро нового тысячелетия (1998–2003)

Второй альбом Complex Numbers.

Музыка: Виктор Аргонов, Александр Асинский, Илья Пятов, Фрол Запольский. Текст: Виктор Аргонов. Вокал: Наталья Митрофанова.

[Скачать mp3 в zip](http://store.complexnumbers.lenin.ru/(2003)%20%d0%a3%d1%82%d1%80%d0%be%20%d0%bd%d0%be%d0%b2%d0%be%d0%b3%d0%be%20%d1%82%d1%8b%d1%81%d1%8f%d1%87%d0%b5%d0%bb%d0%b5%d1%82%d0%b8%d1%8f.zip)  [Скачать вокал и фонограммы](http://store.complexnumbers.lenin.ru/src_basic_morn.zip)

[Слушать в VK](https://vk.com/audios-23865151?section=playlists&z=audio_playlist-23865151_72225328)

</div></div>

<div class="bandcamp-wrapper"><iframe style="border: 0; width: 100%; height: 504px;" src="https://bandcamp.com/EmbeddedPlayer/album=4117159587/size=large/bgcol=333333/linkcol=E49E11/artwork=none/transparent=true/" seamless>[The Morning of the New Millennium by Complex Numbers](https://complexnumbers.bandcamp.com/album/the-morning-of-the-new-millennium)</iframe></div>

## Треки и участники записи

1. **Эволюция.** Музыка - Илья Пятов
2. **Утро нового тысячелетия.** Музыка - Илья Пятов
3. **Пробуждение [Версия 2003].** Музыка, текст - Виктор Аргонов, вокал - Наталья Митрофанова
4. **Экспансия.** Музыка - Фрол Запольский
5. **Солнечное затмение.** Музыка - Aster
6. **Цветущая земля.** Музыка - Виктор Аргонов
7. **Время.** Музыка - Фрол Запольский, Aster
8. **Бесконечный путь.** Музыка - Виктор Аргонов
9. **Трилогия [Версия 2000].** Музыка - Aster
10. **Неизбежность [Русская версия 2002].** Музыка - Виктор Аргонов, вокал - Наталья Митрофанова
11. **Ещё не поздно начать всё сначала.** Музыка - Илья Пятов
