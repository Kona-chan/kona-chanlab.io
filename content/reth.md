---
title: Техно-симфония "Переосмысляя прогресс"
image: 'http://complexnumbers.ru/images/covers/prog-402.jpg'
---

<div class="row">

<div class="albumcover">

![](/images/covers/prog-402.jpg)

</div>

<div class="albumdescription">

* Часть 1. Конструируя среду
* Часть 2. Конструируя информацию
* Часть 3. Конструируя чувства
* Часть 4. Конструируя эволюцию
* Часть 5. Там, за чертой

[Скачать zip со всеми аудио и видео](http://store.complexnumbers.lenin.ru/(2015)%20%d0%9f%d0%b5%d1%80%d0%b5%d0%be%d1%81%d0%bc%d1%8b%d1%81%d0%bb%d1%8f%d1%8f%20%d0%bf%d1%80%d0%be%d0%b3%d1%80%d0%b5%d1%81%d1%81%20(%d1%82%d0%b5%d1%85%d0%bd%d0%be-%d1%81%d0%b8%d0%bc%d1%84%d0%be%d0%bd%d0%b8%d1%8f).zip)
[torrent video](https://rutracker.org/forum/viewtopic.php?t=4992252)&emsp;
[torrent audio](https://rutracker.org/forum/viewtopic.php?t=5290080)&emsp;

[Commons](https://commons.wikimedia.org/wiki/Category:Complex_Numbers_(musical_group):_%D0%9F%D0%B5%D1%80%D0%B5%D0%BE%D1%81%D0%BC%D1%8B%D1%81%D0%BB%D1%8F%D1%8F_%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B5%D1%81%D1%81)&emsp;
[Wikidata](https://www.wikidata.org/wiki/Q105962295)&emsp;
[VK](https://vk.com/audios-23865151?section=playlists&z=audio_playlist-23865151_56297800)

[English version of this page](/en/reth/)

**Видео на YouTube:**
* Русская версия: [одним видео](https://youtu.be/EpVd-TXDUk4), [по частям](https://www.youtube.com/playlist?list=PLKRzpNmnhPb7kYewUaiClwp14Z9STDAsx)
* English version: [single video](http://www.youtube.com/watch?v=3C2tkQ3743E), [separate parts](https://www.youtube.com/playlist?list=PLKRzpNmnhPb4_gCPjHng3Eb6yFPaFaDbQ)

**Аудио:**
* [Послушать на Яндекс музыке](https://music.yandex.ru/album/2985926)
[Apple Music](https://music.apple.com/ru/album/pereosmyslaa-progress/1043679590)  [Amazon Music](https://www.amazon.com/gp/product/B015U8M87O)
[archive.org](https://archive.org/details/complex-numbers--rethinking-progress)

**Текст:**
* [Русская версия](/rethtext/)   [English version](/en/rethtext/)

</div></div>

<div class="bandcamp-wrapper"><iframe style="border: 0; width: 100%; height: 307px;" src="https://bandcamp.com/EmbeddedPlayer/album=3840020883/size=large/bgcol=333333/linkcol=E49E11/artwork=none/transparent=true/" seamless>[Rethinking Progress by Victor Argonov Project](https://argonov.bandcamp.com/album/rethinking-progress)</iframe></div>

## О произведении

"Переосмысляя прогресс" — это экспериментальное электронное произведение в форме, близкой к симфонической. Оно состоит из четырёх инструментальных частей и финальной песни.

Симфония не является в строгом смысле аудио-произведением. Она снабжена официальным видеосинопсисом, который имеет конкретный сюжет. Это философский рассказ о прошлой и будущей истории отношений человека и технологий: от наивного восхищения "прогрессом ради прогресса", через переосмысление идеалов, через попытки бегства от реальности и череду новых открытий — к реальному духовному преображению человечества.

Формат видеосинопсиса — это совмещение авторской музыки и текста с заимствованными изображениями, которые изначально не были созданы для произведения, но имеют с ним эстетические параллели. Цель выбора такого формата — обзор талантливых работ современных фотографов и художников, а также поддержка свободы цитирования в современной культуре. Изображения включены в обзор на правах свободного использования в информационных и культурных целях. Их авторы указаны в конце каждой части видео.

Мелодии симфонии просты и доступны, но они собраны в единую строгую систему. Все части симфонии имеют единый темп 120 ударов в минуту. Длительность каждой инструментальной части (с паузой в конце) составляет 192 такта (384 секунды), длительность финальной песни — 256 тактов (512 секунд). Общая длительность симфонии составляет 1024 такта (2048 секунд, порядка 34 минут). Каждая часть симфонии состоит из четырёх тем и их вариаций. Некоторые мелодии повторяются в разных частях, некоторые есть только в какой-то одной части. Каждая мелодия и каждая гармония имеют свои конкретные смыслы. Подробнее это описано в разделе "Мелодическая система симфонии".

Аранжировки инструментальных частей выполнены в различных стилях: часть 1 — традиционное техно, часть 2 — нинтендовский 8-бит, часть 3 — пиксельная музыка (стандарт, разработанный автором, см. раздел "Концепция пиксельной музыки"), часть 4 — транс. Финальная песня содержит элементы аранжировок каждой из частей, но с преобладанием транса.

Целевой аудиторией симфонии "Переосмысляя прогресс" являются люди, которым интересна мелодичная и экспериментальная электронная музыка, трансгуманистическая футурология, философия сознания, этики и религии, психология изменённых состояний сознания, а также в целом научная фантастика.

## Исходные материалы

Как и к операм "Легенда о несбывшемся грядущем" и "Русалочка", к симфонии "Переосмысляя прогресс" в будущем будут представлены детальные исходные материалы, открытые для свободного использования. На данный момент, однако, доступны лишь миди-файлы всех частей, а также инструментальная фонограмма и вокальные партии ("акапеллы") финальной песни "Там, за чертой" (части 5). Их можно использовать для создания ремиксов. Файлы представлены по адресу https://gitlab.com/complexnumbers/reth-src. Каждый их них имеет название, состоящее из номера части и дескриптора типа партии: "mid" — миди, "voc" — вокал ("fin" — обработанная вокальная дорожка с эффектами, "raw" — необработанная дорожка), "ins" — инструментальная фонограмма.

## Мелодическая система симфонии

Симфония состоит из 9 базовых тем, большинство из которых имеют по несколько вариаций. Темы делятся на пять ритмических классов и могут ложиться на пять аккордовых последовательностей (см. Схему 0). Если две мелодии в целом аналогичны и отличаются лишь аккордами, то они считаются разными вариациями одной и той же темы.

В симфонии используются пять аккордовых последовательностей: три минорные, одна доминантная и одна мажорная. Они обозначаются пятью цветами спектра: холодными — минор, тёплыми - мажор и доминанта. "Синяя" и "зелёная" гармонии тривиальны и широко распространены в мировой культуре. Они выражают простые эмоции. Три другие, выражающие более сложные эмоции, намеренно заимствованы из конкретных произведений с близкой эстетикой (см. Схему 0). На схеме аккордовые последовательности указаны в тональности C (Am), но реально могут транспонироваться в другие тональности. Это не меняет их смысла.

![](/images/misc/sche0.jpg)  

Каждая инструментальная часть симфонии (части 1-4) состоит из четырёх тем (см. Схему 1). Первая тема — это заглавная минорная тема (P, I, C). С неё начинается часть. Вторая тема — это лейтмотив M, который присутствует в каждой части. Третья тема — это романтическая тема в доминантной или мажорной гармонии (D, S). Четвёртая тема — это синкопированное арпеджио (минорный T и доминантный A). В каждой инструментальной части присутствуют четыре из пяти аккордовых последовательностей — две минорные, мажорная и доминантная.

Вокальная часть симфонии (часть 5) состоит из четырёх солирующих тем (все кантиленные темы симфонии: P, C, D и L) и трёх тем-арпеджио, которые могут использоваться только в аккомпанементе (M, T, A). В части 5 встречаются лишь три аккордовые последовательности ("синяя", "сиреневая" и "оранжевая").

![](/images/misc/sche1.jpg)  

В симфонии нет классического деления на главные, побочные, связующие и заключительные темы, но некоторые параллели с этой системой существуют. В основе композиционной структуры симфонии лежит диалог не между темами, а между гармониями: минорными "сиреневой" и "синей" и доминантной "оранжевой". Темы, сыгранные в "сиреневой" и "синей" гармониях, аналогичны главной теме классической формы, а темы, сыгранные в "оранжевой" гармонии — побочной теме. Темы в "зелёной" и "жёлтой" гармониях находятся "над схваткой". Как правило, роль главной темы играет заглавная минорная тема, а роль побочной — романтическая тема. Но в каждой части к ним дополнительно присоединяются синкопированные арпеджио. В частях 1 и 2 "Марш техносферы" играет в той же гармонии, что и заглавная минорная тема, дополнительно утверждая её напряжённо-пафосное настроение. В частях 3 и 4 "Абсолютный свет", напротив, выступает на "романтической" стороне.

![](/images/misc/sche2.jpg)  

Деление тем на 5 ритмических классов связано с порядком их сочинения (см. Схему 2). Самые простые однотактовые ритмы с незначительными вариациями имеют темы-арпеджио T, A и M. Эти темы написаны одними из первых. Они могут использоваться как сольно, так и аккомпанементом к кантиленным темам.

Более сложен ритм тем I и S. Он заимствован у лейтмотива M, но каждый второй такт в нём модифицирован, поэтому мелодии имеют двухтактовую фразировку. Ещё более сложен ритм кантиленных тем D и L, основа которого взята у темы S, но сильно изменена. Ввиду малого количества нот в такте, эти темы допускают вокальное исполнение. В части 5 они имеют функции припевов. Ещё две кантиленные темы — P и C — написаны независимо от других тем симфонии и имеют максимально свободный ритм. В части 5 они имеют функции запевов.

## Пиксельный синтезатор и концепция пиксельной музыки

Особое место в симфонии "Переосмысляя прогресс" занимает третья часть ("Конструируя чувства"). Она выполнена в новаторской концепции "пиксельной музыки" — звучании, которое невозможно получить на известных существующих синтезаторах. Специально для записи "Конструируя чувства" автором был разработан Пиксельный синтезатор (Pixel Synth), текущую версию которого (2016-12 beta) можно скачать [здесь](http://store.complexnumbers.lenin.ru/pixel_synth_2016-12.dll). Эту dll надо скопировать в ваш каталог с другими vst-плагинами, например, Program Files\Steinberg\VstPlugins. Работа синтезатора тестировалась только в секвенсоре Cubase SX (разные версии) под Windows XP.

<img src="/images/misc/pixsynth2.jpg" style="width: 100%" />

Это сырая рабочая версия синтезатора, в которой есть что дорабатывать. Но трудно сказать, когда она будет обновлена, и пока синтезатор доступен только в этой версии.

Для чего делался Пиксельный синтезатор и в чём состоит концепция пиксельной музыки?

В современной культуре музыкальным аналогом пиксельной графики (так называемого пиксел-арта) считается звук примитивных синтезаторов, использовавшихся в 1980-х годах в таких компьютерах и игровых системах как Nintendo (NES), Atari, Commodore 64, ZX-spectrum и т. д. (так называемый chiptune или 8-bit). Ключевая особенность этого звучания — строгая дискретность амплитуды всех музыкальных импульсов (как правило, это не 8-битные, а 4-битные и даже 1-битные импульсы). "Квадратные" формы этих сигналов на осциллографе напоминают пиксельную графику. Но на деле аналогия между chiptune-музыкой и пиксельной графикой поверхностна, и возможен более строгий музыкальный аналог пиксел-арта, который в качестве эксперимента и был реализован в композиции "Конструируя чувства".

Особенности человеческого слуха таковы, что нами непосредственно воспринимается не амплитудно-временная диаграмма сигнала, а его спектр (меняющийся во времени, но медленно в сравнении с самим колебаниями). Поскольку пиксельная графика подразумевает строгую дискретизацию по всем видимым параметрам (крупная сетка пространственных координат, малое количество цветов, хорошо отличимых друг от друга), строгий пиксельный звук должен, аналогично, иметь строго дискретную спектрограмму. Пиксельный звук должен удовлетворять следующим трём принципам.

* Принцип 1. Дискретность по частоте. В сигнале присутствуют только компоненты с частотами определённых значений, хорошо различающиеся между собой на слух (частоты должны отличатся хотя бы на несколько процентов).
* Принцип 2. Дискретность по амплитудам. Амплитуда каждой из компонент может принимать лишь небольшое количество различных значений. Изменение амплитуды даже на минимальную величину должно быть хорошо заметным на слух (минимальный скачок громкости должен составлять хотя бы порядка децибела).
* Принцип 3. Дискретность по времени. Изменения спектра (изменения амплитуд частотных компонент) могут происходить только в определённые моменты времени, интервал между которыми фиксирован и хорошо различим на слух (не менее 1/20 секунды).

В полном музыкальном аналоге пиксельной графики необходимы все три принципа, тогда как большинство chiptune-музыки полноценно реализует только один (дискретность по амплитуде). Дискретность по времени в ней выполняется со слишком малым временным шагом порядка 1/50 секунды (на пределе временного разрешения слуха), а дискретность по частоте не выполняется вовсе (звук содержит огромное количество обертонов, большинство из которых в верхней части спектра сливаются в сплошной шум).

Подавляющее большинство существующих музыкальных инструментов неприменимы для полноценной реализации принципов пиксельной музыки. Некоторые аддитивные синтезаторы допускают "сборку" сигнала из отдельных гармоник. Например, можно сгенерировать звук, состоящий из шести гармоник с частотами 55, 110, 165, 220, 275, 330 Гц и амплитудами, кратными 6, 5, 4, 3, 2, 1. Но при попытке изменить громкость такого сигнала принцип дискретности амплитуд (Принцип 2) будет нарушен. Например, если сделать сигнал вдвое тише, то амплитуды 2-й, 4-й и 6-й гармоник станут дробными. Ещё большие проблемы возникнут при попытке наложить на сигнал эхо или иные эффекты.

Именно ввиду этих сложностей для реализации пиксельной музыки нам потребовался специализированный [Пиксельный синтезатор](http://store.complexnumbers.lenin.ru/pixel_synth_2016-12.dll). Принципы пиксельной музыки в нём реализованы в следующей форме.

* Каждый звук (нота) состоит из набора синусоид с частотами темперированного музыкального звукоряда. Гармоники, выходящие за его рамки (например, гармоники с номерами 7, 14) не допускаются.
* Каждая из синусоид может иметь амплитуды с дискретными значениями от 0 до 15.
* Амплитуды синусоид могут меняться только в дискретные моменты времени через 1/32 ноту.

Звуки синтезатора определяются 7 настройками и массивами настроек, некоторые из которых управляются миди-контроллерами.

* Volume — общая громкость, может иметь 16 градаций, управляется контроллером 7.
* Сut — общая яркость, может иметь 16 градаций, управляется контроллером 74.
* Res — резонанс фильтра, может иметь 16 градаций, но обычно на практике требуются лишь несколько нижних.
* Amplitude Enevelope — кривая изменения громкости со временем.
* Frequency Enevelope — кривая изменения яркости со временем.
* Harmonics — главная настройка, которая определяет тембр звучания. В данной версии не допускает редактирование мышью, допускается только выбор из пресетов. Максимальное число гармоник — 24.
* Formant Filter — многополосный эквалайзер, который, в отличие от внешних обработок, оставляет звук дискретным по всем амплитудам.

Как показал пока ограниченный опыт использования Пиксельного синтезатора, он хорошо имитирует основные chiptune-звуки (симметричный и асимметричный прямоугольники, 4-битный треугольник) а также некоторые традиционные инструменты (орган, клавесин, бас-гитару). При этом, общее звучание значительно отличается от chiptune, будучи более "чистым", "стеклянным" и менее "жирным". Большой проблемой оказалась имитация ударных инструментов, так как они обычно либо имеют сплошной спектр ("шипение" "рабочего" барабана), либо содержат быстрые временные изменения (удар басового барабана). Единственный ударный инструмент, пиксельную имитацию которого на данный момент можно считать удовлетворительной — это хэт.

Система пресетов в данной версии синтезатора разработана лишь для спектров, а не для целых полноценных патчей. Например, есть пресеты синусоиды (sine), симметричного прямоугольника (sqr 1/2), асимметричных прямоугольников (sqr 1/4, sqr 1/8). Звуки типа клавесина в готовом виде в пресетах отсутствуют, их надо настраивать самостоятельно. Впрочем, это несложно. Например, для звука клавесина форма волны должна быть "sqr 1/4", а в окне Amplitute Enevelope - сначала резкое, а затем постепенное затухание звука. Прочие настройки должны стоять по умолчанию. А чтобы сделать орган, надо выбрать форму волны "tri 4 bit" и на нём дублировать все партии в 2-3 октавы.

На рисунке выше приведён пример, как пиксельный синтезатор имитирует звук клавесина. В верхнем центральном окне приведён спектр исходного сигнала (без учёта временного развития). В него включены гармоники, удовлетворяющие двум условиям: (1) их номер не кратен четырём (так построен спектр асимметричного прямоугольного импульса в 8-бит музыке, так называемого "pulse"), (2) они хотя бы приближённо входят в темперированный звукоряд (это отличает данный сигнал от "pulse"). Итого, спектр сигнала состоит из гармоник с номерами 1(C0), 2(C1), 3(G1), 5(E2), 6(G2), 9(D3), 10(E3), 15(H3), 17(C#4), 18(D4), 19(D#4)... Амплитуды гармоник, как и в прямоугольных импульсах, обратно пропорциональны их номерам, но с округлением до 16 градаций громкости. Первая гармоника имеет громкость 15, вторая — 8, третья — 5, пятая и шестая — 3 и т. д. На слух этот звук имеет звонкий и металлический характер (в большей степени, чем обычный "pulse"). В верхнем левом окне построен график зависимости громкости сигнала от времени (32 временных шага, то есть 1 такт). Каждая из гармоник преобразуется независимо от других. Например, первая гармоника меняет громкость строго в соответствие с графиком, а верхние гармоники, изначально имеющие амплитуды 1, сохраняют единичную громкость в течение первых четырёх шагов, а затем просто выключаются. В правом окне приведён цветной спектр конечного сигнала с учётом развития во времени. По вертикали отложена частота (нота звукоряда), по горизонтали — время. Цветом от чёрного через синий, зелёный и жёлтый к белому показана амплитуда гармоники. Как видно из изображения, быстрее всего затухают до нуля тихие верхние гармоники. Это обеспечивает глухой "хвост", естественный для струнных инструментов, но невозможный в рамках chiptune-музыки.

При написании композиции "Конструируя чувства" было выбрано дополнительное ограничение: не должно звучать одновременно более 12 нот. Это ограничение является достаточно мягким: ведь если все эти ноты будут иметь громкие совпадающие гармоники, то число градаций амплитуды итогового сигнала достигнет 181 (что нивелирует Принцип 2). Но на практике такие ситуации не возникали, зато большое число каналов оказалось необходимым для сочного звука с эхом и длинным затуханием нот (например, при имитации звука клавесина, играющего арпеджио). Реальное число используемых градаций громкости даже в басовой области (где некоторые инструменты на полную громкость играют в унисон) почти нигде не превышает 31. Это вполне соответствует Принципу 2.

В финальном варианте "Конструируя чувства" имело место ещё одно смягчение Принципа 2. Каждая частотная компонента в течение звучания трека действительно имеет лишь 16 дискретных значений амплитуды. Но у двух разных частотных компонент конкретный набор значений амплитуд может отличаться. Например, сигнал в области частот 60-70 Гц при прочих равных условиях звучит на 3 децибела громче, чем сигнал в области частот 200-300 Гц. Эта перенормировка (коррекция амплитудно-частотной характеристики при мастеринге) была сделана, чтобы скомпенсировать неравномерность человеческого слуха в разных частотных областях и приблизить звучание композиции к звучанию других частей симфонии. Вариант композиции "Конструируя чувства" без мастеринга (с одинаковой калибровкой амплитуд всех частотных компонент) можно скачать [здесь](http://store.complexnumbers.lenin.ru/cr/part_3_constructing_feelings_raw.mp3).

Как и любая новая технология, технология пиксельной музыки нуждается в практической "обкатке". Нужна наработка новых пресетов для синтезатора, поиск особых приёмов его использования. Возможно, что идея пиксельной музыки окажется чисто экспериментальной. Но возможно, она позволит создать большой новый музыкальный стиль, подобный популярному ныне стилю chiptune (8 bit).

## История написания

2007\. Обсуждение концепции техно-симфонии и вообще крупной формы в танцевальной музыке.

Завершив работу над оперой "2032" и начав работу над новой оперой "Русалочка", Виктор Аргонов размышляет над темой крупной формы в танцевальной электронной музыке. Он задаётся вопросом, почему до сих пор не существует ни одной техно-симфонии (см., например, [обсуждение](
http://klem.ru/forum/showthread.php?t=10144) на форуме Клем). Но дальше обсуждений дело не идёт. Виктор не считает себя достаточно компетентным, чтобы создать такое произведение. Ведь в отличие от оперы, которая может быть связана просто сюжетом, симфония должна быть цельной чисто музыкально.

2010\. Идея пиксельной музыки. Новые идеи о музыкальном единстве произведений крупной формы.

К Виктору приходит идея пиксельной музыки — более точного музыкального аналога графического пиксел-арта, чем существующая музыка в стиле "8-бит" (см. выше раздел "Концепция пиксельной музыки"). Он делает первые опыты по созданию такой музыки на существующих vst-синтезаторах, получает первое представление о её звучании, но понимает, что без специализированного синтезатора её полноценно не реализовать. В этот период продолжается интенсивная работа по записи оперы "Русалочка", и Виктор не может уделить новой идее заметного времени.

Виктор знакомится с дирижёром и философом [Михаилом Аркадьевым](http://arkadev.com/), общение с которым влияет на его идеи о музыкальной целостности и драматургии. Виктор склоняется к мысли, что в цельном произведении не должно быть слишком много тем, важнее разнообразие их вариаций и идейная наполненность. Хорошими примерами здесь служат произведения Вагнера с их системой лейтмотивов (каждая мелодия имеет конкретный смысл), и ["Concerto Grosso"](http://www.youtube.com/watch?v=Ro3Jq561wRI) самого Михаила Аркадьева (всё произведение построено на вариациях лишь двух тем). У Виктора возникает идея применить нечто подобное в "Русалочке", но он понимает, что делать это уже поздно. В ней уже слишком много тем, и не всем им можно приписать однозначный смысл. В опере "Русалочка" Виктор решает уделить больше внимания единству аранжировки. Если у Вагнера смысловыми единицами являются темы, то в "Русалочке" таковыми становятся звуки инструментов: фортепиано — мир людей, арфа — мир русалок, клавесин — сказка, электронные звуки, в зависимости от стиля звучания — наука, технология, космос и т. д. (окончательно эта система отшлифовывается в 2013 году). Идеи же мелодического единства произведения будут полноценно реализованы Виктором лишь в будущей симфонии (см. выше раздел "Мелодическая система симфонии").

2012\. Написание пробной модели синтезатора для пиксельной музыки.

Несмотря на продолжающуюся работу над "Русалочкой", Виктор решает выделить время на написание пиксельного синтезатора в среде Synthmaker. Ему удаётся достичь определённого успеха и убедиться, что такой подход принципиально эффективнее использования готовых синтезаторов. Виктор задумывается о создании серии пиксельных композиций. Однако, быстро написать композицию для нового (во многом, недоведённого) синтезатора не получается. Разработка пиксельной музыки снова откладывается.

2013\. Старт работы над симфонией. Готовность её концепции и большинства мелодий. Высокая интенсивность работ.

<div class="row">

<div class="photocol">

![Виктор Аргонов](/images/photos/vic_31.jpg)  

![Елена Яковец](/images/photos/lena2.jpg)  

![Ariel](/images/photos/ari_23.jpg)  

![Len](/images/photos/ren_2.jpg)  

![Саян Андриянов](/images/photos/say_2.jpg)

</div>

<div class="col">

В феврале, во время финальных доработок "Русалочки", у Виктора возникает мысль, что звучание арии "18 Здесь родится мир" могло бы стать основой первой части будущей техно-симфонии. К этому времени, имея новые представления о музыкальной целостности и символизме, он решает, что уже способен реализовать идею. Быстро рождается концепция формы произведения: темп 120 ударов в минуту; первая часть в стиле техно, вторая — 8бит, третья — пиксельная музыка, четвёртая — транс; равная длительность каждой части 256 секунд (суммарная длительность порядка 17 минут); строго ограниченный, но богато проварьированный набор тем. Симфонию предполагается выпускать под маркой Complex Numbers и использовать в ней не только свои мелодии, но и мелодии соавторов. В марте-апреле Виктор устраивает в своём блоге [платное голосование по выбору своего дальнейшего направления работ](https://argonov.livejournal.com/128018.html), где одним из пунктов является написание симфонии и доведение "до ума" пиксельного синтезатора. В результате голосования, побеждает именно этот пункт.

В мае Виктор начинает интенсивную работу по написанию мелодий для симфонии и поиску соавторов. В течение месяца удаётся написать или заложить основы для несколько удачных мелодий, но поиск соавторов оканчивается безрезультатно. Виктор решает писать симфонию сам под маркой Виктор Аргонов Project. В концепции симфонии появляется важный новый элемент: видеосинопсис с философско-документальным текстом об истории отношений человека и технологий. Появляется и предварительное название симфонии: "Пересекая черту". Становится ясно, что симфонии "тесно" в изначально заявленном карликовом формате. Виктор решает увеличить длины частей с 256 до 384 секунд (общая длина симфонии должна составить порядка 25 минут). В июне появляется первый "пробный" трек в бета-версии — "Часть 2. Конструируя информацию". Впрочем, сильно неформатный 8-битный звук встречает полярную реакцию слушателей.

К ноябрю написаны большинство тем симфонии. Готова бета-версия её первой части "Конструируя среду" с обещанным видеосинопсисом. Появляется идея написания пятой, песенной, части "Там, за чертой". Эта часть должна длиться 512 секунд, а вся симфония — 2048 секунд (порядка 34 минут), то есть вдвое больше изначальной задумки. Виктор обдумывает возможность участия в финальной песне Елены Яковец, запомнившейся слушателям по опере "2032" (в частности, по песне "Зима"), но не участвовавшей в опере "Русалочка". Также планируется участие основной солистки проекта — Ariel.

По состоянию на конец года представлены бета-версии частей 1 и 2, из которых часть 1 — с видеосинопсисом.

2014\. Работа над видео большинства частей. Запись части 5. Готовность полной бета-версии. Высокая интенсивность работ.

В первой половине года дорабатываются оставшиеся инструментальные части, делаются видеосинопсисы к ним. Пишется музыка и текст пятой части. К концу мая в бета-версии представлены все четыре инструментальные части с видеосинопсисами. Летом и осенью записывается вокал пятой части, в ней участвуют три вокалистки: Ariel (солистка Complex Numbers и опер "2032" и "Русалочка"), Елена Яковец (солистка оперы "2032"), а также Len (лидер кавер-проекта [Harmony Team](https://vk.com/harmonyteam)).

31 октября готова бета-версия всей симфонии с видеосинопсом. Начата работа над финальной редакцией произведения. Виктор приводит в порядок композиционную структуру произведения, а также существенно переделывает видео частей 1 и 2, приводя их к единым стандартам оформления и адаптируя к обновлённой музыкальной структуре.

2015\. Готовность финальной версии. Высокая интенсивность работ

В начале года продолжается интенсивная работа над финальной версией симфонии. В январе к проекту в качестве звукорежиссёра присоединяется Саян Андриянов (лидер этно-проекта [Буготак](https://ru.wikipedia.org/wiki/%C1%F3%E3%EE%F2%E0%EA_%28%E3%F0%F3%EF%EF%E0%29)). Он существенно улучшает звук частей 1, 4 и 5. Виктор дорабатывает видеосинопсис частей 3-5. В апреле он принимает решение переименовать произведение в "Переосмысляя прогресс". Во-первых, потому что старое название "Пересекая черту" почти ничего не говорит о содержании произведения, тогда как новое название отражает его довольно точно. Во-вторых, потому что название "Пересекая черту" уже неоднократно использовано другими авторами в кино, литературе и даже игровой индустрии.

</div></div>

19 апреля готова официальная версия симфонии с русскоязычным видеосинопсисом. Этот день считается датой официального электронного релиза симфонии. 10 июня готова англоязычная версия видеосинопсиса. Переводчики — Adhal (лидер металл-проекта [Kachangelion](http://kachangelion.bandcamp.com)) и Андрей Копылов.

В августе [BananaFox](https://vk.com/bananafoxpub) сделал короткие промо-ремиксы в стиле хаус на Пролог и Диалог песни "Там за чертой", а в октябре Денис Мурзин (лидер всероссийски известного электронного проекта, обладателя премии "Золотой граммофон" [DIP Project](http://promodj.com/dipproject)) написал классический трансовый ремикс на всю песню "Там за чертой" (качаем [zip-архив с ремиксами](http://store.complexnumbers.lenin.ru/%d0%9a%d0%b0%d0%b2%d0%b5%d1%80%d1%8b%20%d0%bd%d0%b0%20%d0%bd%d0%b0%d1%81%20(2015-2016).zip)). В сентябре была доделана официальная обложка симфонии от Владимира Горбова и Виктории Неманежиной (студия [Yamato Design](http://yamato-design.ru)).

На фото — Виктор Аргонов, 2013, Елена Яковец, 2013, Ariel, 2012, Len, 2012, Саян Андриянов, 2010.

2016\. Публичный выпуск пиксельного синтезатора

В конце 2016 года вниманию публики представлен [Пиксельный синтезатор](http://store.complexnumbers.lenin.ru/pixel_synth_2016-12.dll), использованный в третьей части симфонии ("Конструируя информацию").
