---
title: 'Последнее кольцо — Complex Numbers'
description: Третий альбом Complex Numbers.
image: 'http://complexnumbers.ru/images/covers/la402r.jpg'
---

<div class="row">
<img src="/images/covers/la402r.jpg" class="albumcover-small" width="256px" />

<div class="albumdescription">

### Последнее кольцо (2007–2009)

Третий альбом Complex Numbers.

Музыка: Виктор Аргонов, Андрей Климковский, Александр Асинский, Hydrogen. Текст: Виктор Аргонов, Ariel. Гитарные партии: Александр Асинский. Вокал: Ariel, Наталья Митрофанова.

[Скачать mp3 и видео в zip](http://store.complexnumbers.lenin.ru/(2009)%20%d0%9f%d0%be%d1%81%d0%bb%d0%b5%d0%b4%d0%bd%d0%b5%d0%b5%20%d0%ba%d0%be%d0%bb%d1%8c%d1%86%d0%be.zip)  [Скачать вокал и фонограммы](http://store.complexnumbers.lenin.ru/src_basic_last.zip)

[Слушать главную песню альбома на Commons](https://commons.wikimedia.org/wiki/File:Complex-numbers--last-ring.opus)

[Слушать в VK](https://vk.com/audios-23865151?section=playlists&z=audio_playlist-23865151_72225350)

</div></div>

<div class="bandcamp-wrapper"><iframe style="border: 0; width: 100%; height: 504px;" src="https://bandcamp.com/EmbeddedPlayer/album=3547327098/size=large/bgcol=333333/linkcol=E49E11/artwork=none/transparent=true/" seamless>[Last Ring by Complex Numbers](https://complexnumbers.bandcamp.com/album/last-ring)</iframe></div>

## Треки и участники записи

1. **Последнее кольцо [Вокальная версия].** Музыка - Андрей Климковский, Виктор Аргонов, текст - Виктор Аргонов, вокал - Ariel
2. **Ускорение [Версия 2007].** Музыка - Aster
3. **Цветные коробки.** Музыка, текст - Виктор Аргонов, вокал - Ariel
4. **KA-52 Аллигатор.** Музыка - Hydrogen, Виктор Аргонов
5. **Инновационные горизонты.** Музыка - Виктор Аргонов
6. **Трилогия. Часть 2 [Версия 2007].** Музыка - Aster
7. **Пути цивилизаций [Версия 2007].** Музыка - Фрол Запольский
8. **Inevitabilite [2009 french version].** Музыка - Виктор Аргонов, текст - Виктор Аргонов, Ariel, вокал - Ariel ([c английскими субтитрами](/swf/inevitabilite-en-fr/)/[swf](/swf/files/inevitabilite-en-fr.swf)))
9. **Неизбежность [Русская версия 2009].** Музыка, текст - Виктор Аргонов, вокал - Наталья Митрофанова ([c русскими субтитрами](/swf/inevitability-ru-ru/)/[swf](/swf/files/inevitability-ru-ru.swf), [c английскими субтитрами](/swf/inevitability-en-ru/)/[swf](/swf/files/inevitability-en-ru.swf))
10. **Последнее кольцо [Инструментальная версия].** Музыка - Андрей Климковский, Виктор Аргонов
