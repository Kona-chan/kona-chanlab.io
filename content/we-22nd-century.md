---
title: 'Мы, XXII век. Техно-опера — Complex Numbers'
description: Четвёртый альбом Complex Numbers.
image: 'http://localhost:3000/images/covers/we402.jpg'
---

<div class="youtube-wrapper">
<iframe width="560" height="315" src="https://www.youtube.com/embed/YrXk2buqsgg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

[English version](https://www.youtube.com/watch?v=zC1o9CjeefI)&emsp;&emsp;&emsp;
[中文版](https://www.bilibili.com/video/BV1Vf4y1t7V3)


Аудиоверсии также доступны на сайтах [bandcamp](https://complexnumbers.bandcamp.com/album/we-22nd-century-electronic-opera), [Spotify](https://open.spotify.com/album/6j6061PFWpOcSXT7EuP87y), [Apple Music](https://music.apple.com/album/1492675036), [Яндекс.Музыка](https://music.yandex.ru/album/9515499), [VK](https://vk.com/music/playlist/-23865151_83082486), [archive.org](https://archive.org/details/complex-numbers-xxii-.-2018-10-05-yr-xk-2buqsgg), [deezer](https://deezer.com/album/124252082), [Amazon](https://www.amazon.com/dp/B0831CXYG8).

[Скачать mp3 и видео в zip](http://store.complexnumbers.lenin.ru/(2018)%20%d0%9c%d1%8b,%20XXII%20%d0%b2%d0%b5%d0%ba%20(%d1%82%d0%b5%d1%85%d0%bd%d0%be-%d0%be%d0%bf%d0%b5%d1%80%d0%b0).zip)&emsp;&emsp;&emsp;
[видео на Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Complex_Numbers_-_%D0%9C%D1%8B,_XXII_%D0%B2%D0%B5%D0%BA._%D0%A2%D0%B5%D1%85%D0%BD%D0%BE-%D0%BE%D0%BF%D0%B5%D1%80%D0%B0_(%D1%84%D0%B8%D0%BD%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F_%D1%80%D1%83%D1%81%D1%81%D0%BA%D0%B0%D1%8F_%D0%B2%D0%B5%D1%80%D1%81%D0%B8%D1%8F_2018-10-05)-YrXk2buqsgg.webm)&emsp;&emsp;&emsp;
[видео/flac-файлы](https://drive.google.com/drive/folders/1PKD2x4AtexAFIhWGcspWRLw_5Z7mLlmP)
[вокал и фонограммы](https://gitlab.com/complexnumbers/we22-src/-/archive/master/we22-src-master.zip), [исходники в git](https://gitlab.com/complexnumbers/we22-src)

## Треки и участники записи

1. **Годы пройдут.** Музыка - Андрей Климковский, Виктор Аргонов, аранжировка, текст - Виктор Аргонов, вокал - Len
2. **Ты мечтал.** Музыка - Виктор Аргонов, Андрей Климковский, аранжировка, текст - Виктор Аргонов, вокал - Ariel
3. **Воздух снов.** Музыка - Виктор Аргонов, Андрей Климковский, аранжировка, текст - Виктор Аргонов, вокал - Ariel
4. **Ты в мире один.** Музыка - Виктор Аргонов, Андрей Климковский, аранжировка, текст - Виктор Аргонов, вокал - Len

## Аннотация

**Мы, XXII век** — электронная опера, четвёртый альбом проекта «Complex Numbers».

Рассказ о человеке XX века, попавшем в XXII век через криозаморозку. Общество, в которое он попал, считает себя утопией и раем на Земле. Но с позиции ценностей XX века оно имеет ряд черт, будто нарочно взятых из антиутопических романов. Главная антиутопическая черта нового общества - разрушение понятия личности и индивидуальности. Разрушение столь глубинное и хитроумно теоретически обоснованное, что Замятин, Хаксли и Оруэлл курят в сторонке. В отличие от диссидентов прошлого, нашего героя никто не думает карать за инакомыслие или даже просто обманывать. Ему честно рассказывают и показывают красоты нового мира. Возможно, показывают с небольшим нарушением личных границ. Но так ли плохо нарушать то, чего нет? Возможен ли вообще для человечества иной сценарий после разгадки тайны разделения мира на "я" и "не я"? Не является ли это разделение лишь временным эволюционным костылём?

Подробное описание истории записи и пояснение смысла оперы доступны на странице [https://argonov.livejournal.com/214695.html](https://argonov.livejournal.com/214695.html).
