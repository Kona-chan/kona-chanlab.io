---
title: 'Complex Numbers: музыка и видео'
---

Все готовые альбомы Complex Numbers включая Виктор Аргонов Project можно не только скачать с этого сайта, но и послушать или купить на таких популярных ресурсах как Яндекс Музыка, YouTube Music и т. д. Complex Numbers и Виктор Аргонов Project на них пока идут в отдельных разделах.

<div class="row">
<div class="albumcover"><img src="/images/misc/itunes.jpg" width="256px" /></div>

<div class="albumdescription">

Яндекс Музыка:  [Виктор Аргонов Project](https://music.yandex.ru/artist/3945844/albums)  [Complex Numbers](https://music.yandex.ru/artist/4526472/albums)  
YouTube Music:  [Виктор Аргонов Project](https://music.youtube.com/channel/UC11exyxN9w5hiovLR2Hj9cw)  [Complex Numbers](https://music.youtube.com/channel/UCP-dQVEUBkb3DpKanlThT-A)  
iTunes:  [Виктор Аргонов Project](https://music.apple.com/ru/artist/%D0%B2%D0%B8%D0%BA%D1%82%D0%BE%D1%80-%D0%B0%D1%80%D0%B3%D0%BE%D0%BD%D0%BE%D0%B2-project/847763834)  [Complex Numbers](https://music.apple.com/ru/artist/complex-numbers/1140302618)  
Amazon Music:  [Виктор Аргонов Project](https://www.amazon.com/s?k=%D0%92%D0%B8%D0%BA%D1%82%D0%BE%D1%80+%D0%90%D1%80%D0%B3%D0%BE%D0%BD%D0%BE%D0%B2+Project&i=digital-music-album&bbn=324381011&dc&qid=1623418170&rnid=14392235011)  [Complex Numbers](https://www.amazon.com/s?k=%22Complex+Numbers%22+%22Zlaxy%22%7C%22Neane%22&i=digital-music-album&bbn=324381011&dc&qid=1623418213&rnid=625149011)  
Bandcamp:  [Виктор Аргонов Project](https://argonov.bandcamp.com/)  [Complex Numbers](https://complexnumbers.bandcamp.com)  
Spotify:  [Виктор Аргонов Project](https://open.spotify.com/artist/1F6ruIf3AI6gL0p0ipxZOT)  [Complex Numbers](https://open.spotify.com/artist/3PBUz26WYPxZo1uFbS2RbV)  
RuTracker:  [Виктор Аргонов Project](https://rutracker.org/forum/viewtopic.php?t=5345848)

Bandcamp содержит лишь наши старые вещи. Будет обновлено. Ссылка на RuTracker — официальная авторская раздача, но лишь для Виктор Аргонов Project. Будет обновлена для всей консолидированной Complex Numbers.

Также в данный момент ведётся работа над [списком всех треков](/tracks/), что в дальнейшем даст возможность послушать все композиции (включая некоторые бета-версии) непосредственно на сайте.

</div>
</div>
