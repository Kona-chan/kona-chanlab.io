---
title: 'Сказки добрые. Каверы-переосмысления — Complex Numbers'
image: 'http://complexnumbers.ru/images/covers/good-tales.jpg'
---

<div class="row">  
<img src="/images/covers/good-tales.jpg" class="albumcover-small" width="256px" />
<div class="albumdescription">

### Сказки добрые. Каверы-переосмысления (2003–2021)

11 треков разных лет, в том числе со страницами обсуждений:  
Сказки добрые (Каролина Trance Cover)  [История и обсуждение](https://argonov.livejournal.com/202947.html)  
8 минут (Игорь Николаев Trance Cover)  [История и обсуждение](https://argonov.livejournal.com/228740.html)

Переработка музыки и текстов: Виктор Аргонов. Вокал: Ariel, Len, Oleviia

[Скачать mp3 в zip](http://store.complexnumbers.lenin.ru/2021-good-fairy-tales.zip)       [VK](https://vk.com/wall-23865151_34082)

</div></div>

<div class="bandcamp-wrapper"><iframe style="border: 0; width: 100%; height: 504px;" src="https://bandcamp.com/EmbeddedPlayer/album=369869619/size=large/bgcol=333333/linkcol=E49E11/artwork=none/transparent=true/" seamless>[Good Fairy-Tales by Complex Numbers](https://complexnumbers.bandcamp.com/album/good-fairy-tales)</iframe></div>

## Треки и участники записи

1. **Сказки добрые (В год пред вспышкой) [Каролина Trance Cover].**  
   Музыка, текст - Сергей Туманов, Виктор Аргонов, аранжировка - Виктор Аргонов, Денис Мурзин, вокал - Ariel
2. **Прекрасное далёко [Крылатов-Энтин Trance Cover].**  
   Музыка - Евгений Крылатов, Виктор Аргонов, текст - Юрий Энтин, вокал - Олевия Кибер
3. **Choral Prelude (Solaris Ocean) [JS Bach Trance Cover].**  
   Музыка - Johan Sebastian Bach, Виктор Аргонов
4. **Wedding March (Post-Wagnerian World) [2021 R Wagner Nintendo & Electro Cover].**  
   Музыка - Richard Wagner, Виктор Аргонов
5. **8 минут (Тысяч солнц прощальный крик) [Игорь Николаев Trance Cover].**  
   Музыка, текст - Игорь Николаев, Виктор Аргонов, вокал - Len
6. **Призрачный рай [Unreal Space Cover].**  
   Музыка - Виктор Аргонов, Futurist, текст - Futurist, вокал - Len
7. **All the Things She Said (Post-Industrial Changes) [2021 tATu Trance Cover].**  
   Музыка - Иван Шаповалов, Сергей Галоян, Виктор Аргонов
8. **Wedding March [2013 R Wagner Nintendo Cover].**  
   Музыка - Richard Wagner, Виктор Аргонов
9.  **Newtype [Unreal Trance Cover].**  
    Музыка - Wonder, Futurist, текст - Futurist, вокал - Arashi
10. **Yesterday Man [Elegant Machinery Trance Cover].**  
    Музыка, текст - Elegant Machinery, вокал - Ariel
11. **All the Things She Said (Post-Industrial Changes) [2003 tATu Trance Cover].**  
    Музыка - Иван Шаповалов, Сергей Галоян, Виктор Аргонов
