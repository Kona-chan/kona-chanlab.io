# Complex Numbers Website

Using SvelteKit!

## Developing

In order to start a development server:

```bash
npm install
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

In order to build the static version of website (including PWA), run:

```bash
npm install
npm run build
```

> You can preview the built app with `npm run preview`

## Tips

* Register redirects in `static/_redirects` file to keep backwards compatibility
* Pages with names like `test.en.md` are available under paths like `/en/test/`
* Feel free to use html markup in markdown files
* [HTML to Markdown converter](https://www.browserling.com/tools/html-to-markdown)
