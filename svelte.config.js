import preprocess from 'svelte-preprocess';
import adapter from '@sveltejs/adapter-static';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: [preprocess({
		postcss: true,
		preserve: ['ld+json'],
	})],
	kit: {
		adapter: adapter({
			pages: 'public',
			assets: 'public',
		}),

		// Hydrate the <div id="svelte"> element in src/app.html
		target: '#svelte',
		trailingSlash: 'ignore',

		vite: {
			optimizeDeps: {
				include: [
					'extend',
					'style-to-object',
					'is-buffer',
					'textr',
					'mdurl',
					'parse5',
					'github-slugger',
					'mdast-util-from-markdown > debug',
				],
			},
		},
	},
};

export default config;
